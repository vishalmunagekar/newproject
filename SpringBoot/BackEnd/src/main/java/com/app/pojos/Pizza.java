package com.app.pojos;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "pizzas")
public class Pizza {
	private Integer pid;
	private String pname;
	private PizzaCategory category; //VEG, NONVEG;
	private String image;
	private String description;
	@JsonIgnore
	private List<PizzaType> pizzaTypes = new ArrayList<>();
	
	public Pizza() {
		System.out.println("Pizzas() POJO works...");
	}

	public Pizza(String pname, PizzaCategory category, String image, String description, List<PizzaType> pizzaTypes) {
		super();
		this.pname = pname;
		this.category = category;
		this.image = image;
		this.description = description;
		this.pizzaTypes = pizzaTypes;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getPid() {
		return pid;
	}
	public void setPid(Integer pid) {
		this.pid = pid;
	}

	@Column(length = 60)
	public String getPname() {
		return pname;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}

	@Column(length = 15)
	@Enumerated(EnumType.STRING)
	public PizzaCategory getCategory() {
		return category;
	}
	public void setCategory(PizzaCategory category) {
		this.category = category;
	}

	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@OneToMany(mappedBy = "pizza", cascade = CascadeType.ALL, orphanRemoval=true, fetch = FetchType.EAGER)
	public List<PizzaType> getPizzaTypes() {
		return pizzaTypes;
	}
	public void setPizzaTypes(List<PizzaType> pizzaTypes) {
		this.pizzaTypes = pizzaTypes;
	}

	@Override
	public String toString() {
		return "Pizza [pid=" + pid + ", pname=" + pname + ", category=" + category + ", image=" + image
				+ ", description=" + description + ", pizzaTypes=" + pizzaTypes + "]";
	}
}
