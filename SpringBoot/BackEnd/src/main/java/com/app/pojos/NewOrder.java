package com.app.pojos;

import java.util.Arrays;

import com.app.entity.PizzaForm;

public class NewOrder {
	public PizzaForm[] pizzas;
	public int totalQty;
	public float totalAmt;
	public int addrid;
	public int uid;
	
	public NewOrder() {
		System.out.println("NewOrder() works...");
	}

	public NewOrder(PizzaForm[] pizzas, int totalQty, float totalAmt, int addrid, int uid) {
		super();
		this.pizzas = pizzas;
		this.totalQty = totalQty;
		this.totalAmt = totalAmt;
		this.addrid = addrid;
		this.uid = uid;
	}

	public PizzaForm[] getPizzas() {
		return pizzas;
	}

	public void setPizzas(PizzaForm[] pizzas) {
		this.pizzas = pizzas;
	}

	public int getTotalQty() {
		return totalQty;
	}

	public void setTotalQty(int totalQty) {
		this.totalQty = totalQty;
	}

	public float getTotalAmt() {
		return totalAmt;
	}

	public void setTotalAmt(float totalAmt) {
		this.totalAmt = totalAmt;
	}

	public int getAddrid() {
		return addrid;
	}

	public void setAddrid(int addrid) {
		this.addrid = addrid;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	@Override
	public String toString() {
		return "NewOrder [pizzas=" + Arrays.toString(pizzas) + ", totalQty=" + totalQty + ", totalAmt=" + totalAmt
				+ ", addrid=" + addrid + ", uid=" + uid + "]";
	}
}
