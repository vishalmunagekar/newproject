package com.app.pojos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "orders")
public class Order {
	private Integer oid;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date date;
	private float totalAmt;
	private OrderStatus status; //Delivered, NotDelivered;
	@JsonIgnore
	public List<OrderDetails> orderDetails = new ArrayList<OrderDetails>();
	private int totalQty;
	@JsonIgnore
	private User user;
	@JsonIgnore
	private Address oaddr;

	public Order() {
		System.out.println("Orders() POJO works...");
	}

	public Order(Date date, float totalAmt, OrderStatus status,
			int totalQty, User user, Address oaddr) {
		super();
		this.date = date;
		this.totalAmt = totalAmt;
		this.status = status;
		this.totalQty = totalQty;
		this.user = user;
		this.oaddr = oaddr;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getOid() {
		return oid;
	}
	public void setOid(Integer oid) {
		this.oid = oid;
	}

	@Column(length = 15)
	@Enumerated(EnumType.STRING)
	public OrderStatus getStatus() {
		return status;
	}
	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

	public float getTotalAmt() {
		return totalAmt;
	}
	public void setTotalAmt(float totalAmt) {
		this.totalAmt = totalAmt;
	}

	public int getTotalQty() {
		return totalQty;
	}
	public void setTotalQty(int totalQty) {
		this.totalQty = totalQty;
	}

	@OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
	public List<OrderDetails> getOrderDetails() {
		return orderDetails;
	}
	public void setOrderDetails(List<OrderDetails> orderDetails) {
		this.orderDetails = orderDetails;
	}

	@ManyToOne
	@JoinColumn(name="uid")
	public User getuser() {
		return user;
	}
	public void setuser(User user) {
		this.user = user;
	}

	@OneToOne
	@JoinColumn(name="addrid")
	public Address getOaddr() {
		return oaddr;
	}
	public void setOaddr(Address oaddr) {
		this.oaddr = oaddr;
	}
	
	//conviniance method
	public void addOrderDetail(OrderDetails orderDetails) {
		this.orderDetails.add(orderDetails);
	}

	@Override
	public String toString() {
		return "Order [oid=" + oid + ", date=" + date + ", totalAmt=" + totalAmt + ", status=" + status
				+ ", orderDetails=" + orderDetails + ", totalQty=" + totalQty + ", user=" + user + ", oaddr=" + oaddr
				+ "]";
	}
}
