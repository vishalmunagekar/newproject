package com.app.pojos;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "pizza_type")
public class PizzaType {
	private Integer pizTypeId;
	private PizzaAvailabilityStatus status; //AVAILABLE, NOTAVAILABLE
	private PizzaSize size; //PERSONAL, MEDIUM, FAMILY
	private float price;
	@JsonIgnore
	private Pizza pizza;
	
	public PizzaType() {
		System.out.println("PizzaType() POJO works...");
	}
	
	public PizzaType(PizzaAvailabilityStatus status, PizzaSize size, float price, Pizza pizza) {
		super();
		this.status = status;
		this.size = size;
		this.price = price;
		this.pizza = pizza;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Integer getPizTypeId() {
		return pizTypeId;
	}

	public void setPizTypeId(Integer pizTypeId) {
		this.pizTypeId = pizTypeId;
	}

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "pid")
	public Pizza getPizza() {
		return pizza;
	}
	public void setPizza(Pizza pizza) {
		this.pizza = pizza;
	}

	@Column(length = 15)
	@Enumerated(EnumType.STRING)
	public PizzaAvailabilityStatus getStatus() {
		return status;
	}
	public void setStatus(PizzaAvailabilityStatus status) {
		this.status = status;
	}

	@Column(length = 15)
	@Enumerated(EnumType.STRING)
	public PizzaSize getSize() {
		return size;
	}
	public void setSize(PizzaSize size) {
		this.size = size;
	}

	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "PizzaType [pizTypeId=" + pizTypeId + ", status=" + status + ", size=" + size + ", price=" + price + "]";
	}
}
