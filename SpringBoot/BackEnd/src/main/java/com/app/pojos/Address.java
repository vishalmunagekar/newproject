package com.app.pojos;

import javax.persistence.*;

import com.app.pojos.User;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "address")
public class Address {
	private Integer addrid;
	private String street;
	private int flatno;
	private String city;
	private int pincode;
	@JsonIgnore
	private User user;
	
	
	public Address() {
		System.out.println("Address() POJO works...");
	}
		
	public Address(String street, int flatno, String city, int pincode, User user) {
		super();
		this.street = street;
		this.flatno = flatno;
		this.city = city;
		this.pincode = pincode;
		this.user = user;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Integer getAddrid() {
		return addrid;
	}
	public void setAddrid(Integer addrid) {
		this.addrid = addrid;
	}

	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}

	public int getFlatno() {
		return flatno;
	}
	public void setFlatno(int flatno) {
		this.flatno = flatno;
	}

	@Column(length=60,nullable=false)
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}

	public int getPincode() {
		return pincode;
	}
	public void setPincode(int pincode) {
		this.pincode = pincode;
	}
	
	@ManyToOne
	@JoinColumn(name="uid")
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Address [addrid=" + addrid + ", street=" + street + ", flatno=" + flatno + ", city=" + city
				+ ", pincode=" + pincode + ", user=" + user + "]";
	}
}
