package com.app.pojos;

public enum PizzaSize {
	PERSONAL, MEDIUM, FAMILY
}
