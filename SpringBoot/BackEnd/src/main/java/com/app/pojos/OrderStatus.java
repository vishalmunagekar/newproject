package com.app.pojos;

public enum OrderStatus {
	Delivered, NotDelivered, Canceled;
}
