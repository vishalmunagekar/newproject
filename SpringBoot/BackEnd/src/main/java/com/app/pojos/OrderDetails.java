package com.app.pojos;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "order_details")
public class OrderDetails {
	private Integer id;
	private int quantity;
	private PizzaType pizzaType;
	private float totalPrice;
	@JsonIgnore
	private Order order;
	
	public OrderDetails() {
		System.out.println("OrderDetails() works...");
	}

	public OrderDetails(int quantity, PizzaType pizzaType, float totalPrice, Order order) {
		super();
		this.quantity = quantity;
		this.pizzaType = pizzaType;
		this.totalPrice = totalPrice;
		this.order = order;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public float getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(float totalPrice) {
		this.totalPrice = totalPrice;
	}

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "piz_type_id")
	public PizzaType getPizzaType() {
		return pizzaType;
	}
	public void setPizzaType(PizzaType pizzaType) {
		this.pizzaType = pizzaType;
	}

	@ManyToOne
	@JoinColumn(name = "oid")
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}

	@Override
	public String toString() {
		return "OrderDetails [id=" + id + ", quantity=" + quantity + ", pizzaType=" + pizzaType + ", totalPrice="
				+ totalPrice + ", order=" + order + "]";
	}	
}
