package com.app.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.entity.UserForm;
import com.app.pojos.User;
import com.app.service.IUserService;

@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController
{
	@Autowired
	private IUserService Uservice;
	
	public UserController() {
		System.out.println("UserController() works...");
	}
	
	/*
	 * @GetMapping public ResponseEntity<?> getAllUsers() { List<User> userList =
	 * Uservice.getAllUser(); if(userList.isEmpty()) return new
	 * ResponseEntity<String>("No Users Available...",HttpStatus.NOT_FOUND); return
	 * new ResponseEntity<List<User>>(userList, HttpStatus.OK); }
	 */
	
	@GetMapping("/{uid}")
	public ResponseEntity<?> getUserById(@PathVariable int uid)
	{
		UserForm user = Uservice.getUserById(uid);
		if(user == null)
			return new ResponseEntity<String>("No User Found...", HttpStatus.NOT_FOUND);
		return new ResponseEntity<UserForm>(user, HttpStatus.OK);
	}
	
	@PostMapping("/add")
	public ResponseEntity<?> addNewUser(@RequestBody User u)
	{
		if(Uservice.getUserByEmail(u.getEmail()) != null){
			return new ResponseEntity<String>("Email ID already exist..!!",HttpStatus.CONFLICT);
		}
		else if(Uservice.getUserByMobNo(u.getMobNo()) != null){
			return new ResponseEntity<String>("Mobile No. already exist..!!",HttpStatus.CONFLICT);
		}
		else {
			return new ResponseEntity<User>(Uservice.addUserDetails(u),HttpStatus.OK);
		}
		 
	}
	
	@PutMapping("/update")
	public ResponseEntity<String> updateUser(@RequestBody User u)
	{
		
		if(Uservice.updateUserDetails(u) == null){
			return new ResponseEntity<String>("Somthing went wrong..!",HttpStatus.INTERNAL_SERVER_ERROR);
		}
		else {
			return new ResponseEntity<String>("Your profile has been successfully update...", HttpStatus.OK);
		}
	}
	
	/*
	 * @DeleteMapping("/delete/{uid}") public int deleteUser(@PathVariable int uid)
	 * { Uservice.deleteUserInfo(uid); return 1; }
	 */
	
	@PostMapping("/login")
	public ResponseEntity<?> userAuthentication(@RequestBody User u) {
		UserForm user = Uservice.userAuthentication(u.getEmail(), u.getPass());
		if(user == null)
			return new ResponseEntity<String>("wrong credentials...please try again..!!",HttpStatus.UNAUTHORIZED);
		return new ResponseEntity<UserForm>(user,HttpStatus.OK);
	}
}




