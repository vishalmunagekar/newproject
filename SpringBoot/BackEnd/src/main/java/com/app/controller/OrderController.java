package com.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.entity.OrderForm;
import com.app.pojos.NewOrder;
import com.app.pojos.Order;
import com.app.service.IOrderService;

@RestController
@RequestMapping("/order")
@CrossOrigin
public class OrderController {

	@Autowired
	private IOrderService Oservice;

	public OrderController() {
		System.out.println("Order Controller works...");
	}
	
	@GetMapping
	public List<Order> getAllOrders() {
		return Oservice.getAllOrder();
	}

	@GetMapping("/{oid}")
	public OrderForm getOrderDetails(@PathVariable int oid) {
		return Oservice.getOrderById(oid);
	}

	@PostMapping("/add")
	public int addOrderDetails(@RequestBody NewOrder newOrder) {
		
		//System.out.println(newOrder);
		return Oservice.addOrderDetails(newOrder);
	}

	@PutMapping("/update/{oid}")
	public Order updateOrderDetails(int oid, Order o) {
		return Oservice.updateOrderDetails(oid, o);
	}

	@DeleteMapping("/delete/{oid}")
	public void deleteOrderInfo(int oid) {
		Oservice.deleteOrderInfo(oid);
	}
	
	@GetMapping("/userorders/{uid}")
	public List<OrderForm> getAllOrdersByUserId(@PathVariable int uid)
	{
		return Oservice.getAllOrdersByUserId(uid);
	}
	
	
}
