package com.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.pojos.Address;
import com.app.service.IAddressService;

@RestController
@CrossOrigin//(origins = "*", allowedHeaders = "*")
@RequestMapping("/address")
public class AddressController {
	
	@Autowired
	private IAddressService AService;

	public AddressController() {
		// TODO Auto-generated constructor stub
	}

	@GetMapping
	public List<Address> getAllAddress() {
		return AService.getAllAddress();
	}

	@GetMapping("/{aid}")
	public Address getAddressDetails(@PathVariable int aid) {
		return AService.getAddressDetails(aid);
	}

	@PostMapping("/add/{uid}")
	public Address addAddressDetails( @PathVariable int uid, @RequestBody Address a) {
		return AService.addAddressDetails(uid, a);
	}

	@PutMapping("/update/{aid}")
	public Address updateAddressDetails(@PathVariable int aid, @RequestBody Address a) {
		return AService.updateAddressDetails(aid, a);
	}

	@DeleteMapping("/delete/{aid}")
	public void deleteAddressInfo(int aid) {
		AService.deleteAddressInfo(aid);
	}
	
	@GetMapping("/alladdress/{uid}")
	public List<Address> getAllAddressById(@PathVariable int uid){
		return AService.getAllAddressById(uid);
	} 
}
