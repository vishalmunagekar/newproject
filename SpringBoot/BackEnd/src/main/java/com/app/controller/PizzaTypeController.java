package com.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.entity.PizzaTypeDTO;
import com.app.forms.pizzaTypeForm;
import com.app.pojos.PizzaType;
import com.app.service.IPizzaTypeService;

@RestController
@RequestMapping("/pizzaType")
@CrossOrigin
public class PizzaTypeController {

	@Autowired
	private IPizzaTypeService PTService;

	public PizzaTypeController() {
		System.out.println("PizzaTypeController() Works...");
	}

	@GetMapping
	public List<PizzaType> getAllPizzasTypes() {
		return PTService.getAllPizzaTypes();
	}

	@GetMapping("/{PizTypeId}")
	public pizzaTypeForm getPizzaTypeById(@PathVariable int PizTypeId) {
		return PTService.getPizzaFormTypeById(PizTypeId);
	}
	
	@PutMapping("/update")
	public PizzaType updatePizzaTypeDetails(@RequestBody PizzaTypeDTO newPizzaTpe)
	{
		System.out.println(newPizzaTpe);
		return PTService.updatePizzaTypeDetails(newPizzaTpe);
	}
}