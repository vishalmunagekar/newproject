package com.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.forms.PizzaViewForm;
import com.app.pojos.Pizza;
import com.app.service.IPizzaService;

@RestController
@RequestMapping("/pizza")
@CrossOrigin
public class PizzaController{

	@Autowired
	private IPizzaService Pservice;
	
	public PizzaController() {
		System.out.println("Pizza Controller Works...");
	}
	
	@GetMapping
	public List<PizzaViewForm> getAllPizzaViewForm() {
		return Pservice.getAllPizzaViewForm();
	}

	@GetMapping("/{pid}")
	public Pizza getPizzaDetails(@PathVariable int pid) {
		System.out.println(pid);
		return Pservice.getPizzaDetails(pid);
	}

	@PostMapping("/add")
	public Pizza addPizzaDetails(@RequestBody Pizza o) {
		return Pservice.addPizzaDetails(o);
	}

	@PutMapping("/update")
	public int updatePizzaDetails(@RequestBody Pizza p) {
		//System.out.println(p);
		return Pservice.updatePizzaDetails(p);
	}

	@DeleteMapping("/delete/{pid}")
	public void deletePizzaInfo(@PathVariable int pid) {
		Pservice.deletePizzaInfo(pid);
	}

}
