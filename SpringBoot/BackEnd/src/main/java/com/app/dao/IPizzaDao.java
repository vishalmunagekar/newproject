package com.app.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.forms.PizzaViewForm;
import com.app.pojos.Pizza;

public interface IPizzaDao extends JpaRepository<Pizza, Integer> {
	
	@Query("SELECT p FROM Pizza p")
	List<PizzaViewForm> getAllPizzaViewForm();
	
}