package com.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.forms.pizzaTypeForm;
import com.app.pojos.PizzaType;

public interface IPizzaTypeDao extends JpaRepository<PizzaType, Integer> {
	//pizzaTypeForm findByPizTypeId(Integer pizTypeId);
	@Query("SELECT pt FROM PizzaType pt WHERE pt.pizTypeId = ?1")
	pizzaTypeForm getPizzaTypeById(int pizTypeId);
}
