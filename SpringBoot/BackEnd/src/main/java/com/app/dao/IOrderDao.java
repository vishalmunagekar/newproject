package com.app.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.entity.OrderForm;
import com.app.pojos.Order;

public interface IOrderDao extends JpaRepository<Order, Integer> {
	@Query("SELECT o FROM Order o")
	List<OrderForm> getAllOrders();
	
	@Query("SELECT o FROM Order o WHERE o.user.id = ?1")
	List<OrderForm> getAllOrdersByUserId(int uid);
	
	@Query("SELECT o FROM Order o WHERE o.oid = ?1")
	OrderForm getOrderById(int oid);
}
