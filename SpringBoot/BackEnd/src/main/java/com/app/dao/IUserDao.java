package com.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.pojos.User;
import com.app.entity.UserForm;

public interface IUserDao extends JpaRepository<User, Integer>
{
	@Query("SELECT u FROM User u WHERE u.email = ?1 and u.pass = ?2")
	UserForm userAuthentication(String email, String pass);
	
	@Query("SELECT u FROM User u WHERE u.id = ?1")
	UserForm getUserById(int uid);
	
	@Query("SELECT u FROM User u WHERE u.email = ?1")
	UserForm getUserByEmail(String email);
	
	@Query("SELECT u FROM User u WHERE u.mobNo = ?1")
	UserForm getUserByMobNo(String mobNo);
}
