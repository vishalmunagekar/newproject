package com.app.forms;

import java.util.List;

import com.app.pojos.PizzaCategory;
import com.app.pojos.PizzaType;

public interface PizzaViewForm {
	 Integer getPid();
	 String getPname();
	 PizzaCategory getCategory();
	 String getImage();
	 String getDescription();
	 List<PizzaType> getPizzaTypes();
}
