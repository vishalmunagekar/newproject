package com.app.forms;

import com.app.pojos.Pizza;
import com.app.pojos.PizzaAvailabilityStatus;
import com.app.pojos.PizzaSize;

public interface pizzaTypeForm {
	Integer getPizTypeId();
	PizzaAvailabilityStatus getStatus();
	PizzaSize getSize();
	float getPrice();
	Pizza getPizza();
}
