package com.app.entity;

import java.util.Date;
import com.app.pojos.UserRole;

public interface UserForm {
	Integer getId();
	String getUname();
	String getMobNo();
	Date getDob();
	String getEmail();
	String getPass();
	UserRole getRole();
}
