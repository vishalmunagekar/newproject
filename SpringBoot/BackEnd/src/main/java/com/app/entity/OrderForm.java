package com.app.entity;

import java.util.Date;
import java.util.List;

import com.app.pojos.Address;
import com.app.pojos.OrderStatus;
import com.app.pojos.Pizza;
import com.app.pojos.PizzaAvailabilityStatus;
import com.app.pojos.PizzaSize;

public interface OrderForm {
	Integer getOid();
	OrderStatus getStatus();
	Date getDate();
	float getTotalAmt();
	int getTotalQty();
	List<OrderDetailsForm> getOrderDetails();
	Address getOaddr();
}


interface OrderDetailsForm{
	 Integer getId();
	 int getQuantity();
	 pizzaTypeForm getPizzaType();
	 float getTotalPrice();
} 

interface pizzaTypeForm {
	Integer getPizTypeId();
	PizzaAvailabilityStatus getStatus();
	PizzaSize getSize();
	float getPrice();
	Pizza getPizza();
}