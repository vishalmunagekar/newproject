package com.app.entity;


public class PizzaForm {
	public Integer pizTypeId;
	public int quantity;
	public float totalPrice;
	
	public PizzaForm() {
		System.out.println("PizzaForm() works...");
	}

	public PizzaForm(Integer pizTypeId, int quantity, float totalPrice) {
		super();
		this.pizTypeId = pizTypeId;
		this.quantity = quantity;
		this.totalPrice = totalPrice;
	}

	public Integer getPizTypeId() {
		return pizTypeId;
	}
	public void setPizTypeId(Integer pizTypeId) {
		this.pizTypeId = pizTypeId;
	}

	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public float getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(float totalPrice) {
		this.totalPrice = totalPrice;
	}

	@Override
	public String toString() {
		return "PizzaForm [pizTypeId=" + pizTypeId + ", quantity=" + quantity + ", totalPrice=" + totalPrice + "]";
	}
}
