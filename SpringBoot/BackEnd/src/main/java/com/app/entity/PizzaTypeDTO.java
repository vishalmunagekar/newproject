package com.app.entity;

import com.app.pojos.Pizza;
import com.app.pojos.PizzaAvailabilityStatus;
import com.app.pojos.PizzaSize;

public class PizzaTypeDTO {
	private Integer pizTypeId;
	private PizzaAvailabilityStatus status; //AVAILABLE, NOTAVAILABLE
	private PizzaSize size; //PERSONAL, MEDIUM, FAMILY
	private float price;
	private Pizza pizza;
	
	public PizzaTypeDTO() {
		System.out.println("PizzaTypeDTO() works...");
	}

	public PizzaTypeDTO(Integer pizTypeId, PizzaAvailabilityStatus status, PizzaSize size, float price,
			Pizza pizza) {
		super();
		this.pizTypeId = pizTypeId;
		this.status = status;
		this.size = size;
		this.price = price;
		this.pizza = pizza;
	}

	public Integer getPizTypeId() {
		return pizTypeId;
	}

	public void setPizTypeId(Integer pizTypeId) {
		this.pizTypeId = pizTypeId;
	}

	public PizzaAvailabilityStatus getStatus() {
		return status;
	}

	public void setStatus(PizzaAvailabilityStatus status) {
		this.status = status;
	}

	public PizzaSize getSize() {
		return size;
	}

	public void setSize(PizzaSize size) {
		this.size = size;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public Pizza getPizza() {
		return pizza;
	}

	public void setPizza(Pizza pizza) {
		this.pizza = pizza;
	}

	@Override
	public String toString() {
		return "PizzaTypeDTO [pizTypeId=" + pizTypeId + ", status=" + status + ", size=" + size + ", price=" + price
				+ ", pizza=" + pizza + "]";
	}
}
