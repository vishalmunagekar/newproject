package com.app.entity;

import com.app.pojos.PizzaCategory;

public class PizzaDTO {
	private Integer pid;
	private String pname;
	private PizzaCategory category; //VEG, NONVEG;
	private String image;
	private String description;
	
	public PizzaDTO() {
		System.out.println("PizzaDTO() works...");
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public String getPname() {
		return pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}

	public PizzaCategory getCategory() {
		return category;
	}

	public void setCategory(PizzaCategory category) {
		this.category = category;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "PizzaDTO [pid=" + pid + ", pname=" + pname + ", category=" + category + ", image=" + image
				+ ", description=" + description + "]";
	}
}
