package com.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.dao.IAddressDao;
import com.app.pojos.Address;

@Service
@Transactional
public class AddressServiceImpl implements IAddressService {
	
	@Autowired
	private IAddressDao Adao;
	
	@Autowired
	private UserServiceImpl Uservice;
	
	@Override
	public List<Address> getAllAddress() {
		return Adao.findAll();
	}

	@Override
	public Address getAddressDetails(int aid) {
		return Adao.findById(aid).get();
	}

	@Override
	public Address addAddressDetails(int uid, Address a) {
		a.setUser(Uservice.getUserDetails(uid));
		return Adao.save(a);
	}

	@Override
	public Address updateAddressDetails(int aid, Address a) {
		Address oldadd = Adao.getOne(aid);
		Adao.save(a);
		return a;
	}

	@Override
	public void deleteAddressInfo(int aid) {
		Adao.deleteById(aid);
	}

	@Override
	public List<Address> getAllAddressById(int uid) {
		return Adao.getAllAddressById(uid);
	}

}
