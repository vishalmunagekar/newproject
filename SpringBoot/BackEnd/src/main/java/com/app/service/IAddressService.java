package com.app.service;

import java.util.List;

import com.app.pojos.Address;


public interface IAddressService {
	List<Address> getAllAddress();

	Address getAddressDetails(int aid);

	Address addAddressDetails(int uid, Address a);
	
	Address updateAddressDetails(int aid,Address a);
	
	void deleteAddressInfo(int aid);
	
	List<Address> getAllAddressById(int uid);
}
