package com.app.service;

import java.util.List;

import com.app.entity.OrderForm;
import com.app.pojos.NewOrder;
import com.app.pojos.Order;

public interface IOrderService {
	List<Order> getAllOrder();

	OrderForm getOrderById(int oid);

	int addOrderDetails(NewOrder newOrder);
	
	Order updateOrderDetails(int oid,Order o);
	
	void deleteOrderInfo(int oid);
	
	//List<OrderForm> getAllOrders();
	
	List<OrderForm> getAllOrdersByUserId(int uid);
}
