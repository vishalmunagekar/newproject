package com.app.service;

import java.util.List;

import com.app.entity.UserForm;
import com.app.pojos.User;

public interface IUserService {
	List<User> getAllUser();

	User getUserDetails(int uid);

	User addUserDetails(User u);
	
	User updateUserDetails(User u);
	
	void deleteUserInfo(int uid);
	
	UserForm userAuthentication(String email, String pass);
	
	UserForm getUserById(int uid);
	
	UserForm getUserByEmail(String email);
	
	UserForm getUserByMobNo(String mobNo);
}
