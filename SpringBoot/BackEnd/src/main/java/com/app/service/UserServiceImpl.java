package com.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.dao.IUserDao;
import com.app.entity.UserForm;
import com.app.pojos.User;


@Service
@Transactional
public class UserServiceImpl implements IUserService {
	
	@Autowired
	private IUserDao Udao;
	
	@Override
	public List<User> getAllUser() {
		return Udao.findAll();
	}

	@Override
	public User getUserDetails(int uid) {
		return Udao.findById(uid).get();
	}

	@Override
	public User addUserDetails(User u) {
		return Udao.save(u);
	}

	@Override
	public User updateUserDetails(User u) {
		Udao.getOne(u.getId());
		return Udao.save(u);
	}

	@Override
	public void deleteUserInfo(int uid) {
		Udao.deleteById(uid);
	}

	@Override
	public UserForm userAuthentication(String email, String pass) {
		return Udao.userAuthentication(email, pass);
	}

	@Override
	public UserForm getUserById(int uid) {
		return Udao.getUserById(uid);
	}
	
	@Override
	public UserForm getUserByEmail(String email)
	{
		return Udao.getUserByEmail(email);
	}

	@Override
	public UserForm getUserByMobNo(String mobNo) {
		return Udao.getUserByMobNo(mobNo);
	}
}
