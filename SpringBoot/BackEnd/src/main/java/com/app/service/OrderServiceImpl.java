package com.app.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.dao.IOrderDao;
import com.app.entity.OrderForm;
import com.app.pojos.NewOrder;
import com.app.pojos.Order;
import com.app.pojos.OrderDetails;
import com.app.pojos.OrderStatus;

@Service
@Transactional
public class OrderServiceImpl implements IOrderService {
	
	@Autowired
	private IOrderDao Odao;
	
	@Autowired
	private IPizzaTypeService PTService;
	
	@Autowired
	private UserServiceImpl Uservice;
	
	@Autowired
	private AddressServiceImpl Aservice;
	
	@Override
	public List<Order> getAllOrder() {
		return Odao.findAll();
	}

//	@Override
//	public Order getOrderDetails(int oid) {
//		return Odao.findById(oid).get();
//	}

	@Override
	public int addOrderDetails(NewOrder newOrder) {		
		Order order = new Order(new Date(), 
				newOrder.totalAmt,
				OrderStatus.NotDelivered,
				newOrder.totalQty,
				Uservice.getUserDetails(newOrder.uid), 
				Aservice.getAddressDetails(newOrder.addrid));
		for(int i = 0; i < newOrder.pizzas.length; i++)
		{
			OrderDetails orderDetails = new OrderDetails();
			
			orderDetails.setTotalPrice(newOrder.pizzas[i].totalPrice);
			orderDetails.setQuantity(newOrder.pizzas[i].quantity);
			orderDetails.setPizzaType(PTService.getPizzaTypeById(newOrder.pizzas[i].pizTypeId));
			orderDetails.setOrder(order);
			order.addOrderDetail(orderDetails);
		}
		
		Order persistOrder =  Odao.save(order);
		
		return persistOrder.getOid();
	}

	@Override
	public Order updateOrderDetails(int oid, Order o) {
		Odao.getOne(oid);
		Odao.save(o);
		return o;
	}

	@Override
	public void deleteOrderInfo(int oid) {
		Odao.deleteById(oid);
	}

//	@Override
//	public List<OrderForm> getAllOrders() {
//		return Odao.getAllOrders();
//	}

	@Override
	public List<OrderForm> getAllOrdersByUserId(int uid) {
		return Odao.getAllOrdersByUserId(uid);
	}

	@Override
	public OrderForm getOrderById(int oid) {
		return Odao.getOrderById(oid);
	}

}
