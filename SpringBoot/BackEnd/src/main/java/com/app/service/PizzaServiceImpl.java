package com.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.dao.IPizzaDao;
import com.app.forms.PizzaViewForm;
import com.app.pojos.Pizza;


@Service
@Transactional
public class PizzaServiceImpl implements IPizzaService {

	@Autowired
	private IPizzaDao Pdao;
	
//	@Override
//	public List<Pizza> getAllPizza() {
//		return Pdao.findAll();
//	}
	
	@Override
	public List<PizzaViewForm> getAllPizzaViewForm() {
		return Pdao.getAllPizzaViewForm();
	}

	@Override
	public Pizza getPizzaDetails(int oid) {
		return Pdao.findById(oid).get();
	}

	@Override
	public Pizza addPizzaDetails(Pizza o) {
		return Pdao.save(o);
	}

	@Override
	public int updatePizzaDetails(Pizza p) {
		Pdao.getOne(p.getPid());
		Pdao.save(p);
		return p.getPid();
	}

	@Override
	public void deletePizzaInfo(int pid) {
		Pdao.deleteById(pid);
	}

}
