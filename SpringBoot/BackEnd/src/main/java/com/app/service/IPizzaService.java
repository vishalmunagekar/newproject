package com.app.service;

import java.util.List;

import com.app.forms.PizzaViewForm;
import com.app.pojos.Pizza;

public interface IPizzaService {
	//List<Pizza> getAllPizza();

	Pizza getPizzaDetails(int pid);

	Pizza addPizzaDetails(Pizza pizza);
	
	int updatePizzaDetails(Pizza pizza);
	
	void deletePizzaInfo(int pid);
	
	List<PizzaViewForm> getAllPizzaViewForm();
}
