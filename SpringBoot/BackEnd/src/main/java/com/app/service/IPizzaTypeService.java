package com.app.service;

import java.util.List;

import com.app.entity.PizzaTypeDTO;
import com.app.forms.pizzaTypeForm;
import com.app.pojos.PizzaType;

public interface IPizzaTypeService {
	List<PizzaType> getAllPizzaTypes();
	
	pizzaTypeForm getPizzaFormTypeById(int PizTypeId);
	
	PizzaType getPizzaTypeById(int PizTypeId);
	
	PizzaType updatePizzaTypeDetails(PizzaTypeDTO newPizzaTpe);
}
