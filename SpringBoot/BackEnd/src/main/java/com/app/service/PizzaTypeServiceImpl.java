package com.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.dao.IPizzaDao;
import com.app.dao.IPizzaTypeDao;
import com.app.entity.PizzaTypeDTO;
import com.app.forms.pizzaTypeForm;
import com.app.pojos.Pizza;
import com.app.pojos.PizzaType;


@Service
@Transactional
public class PizzaTypeServiceImpl implements IPizzaTypeService {

	@Autowired
	private IPizzaTypeDao PTDao;
	
	@Autowired
	private IPizzaDao PDao;
	
	@Override
	public List<PizzaType> getAllPizzaTypes() {
		return PTDao.findAll();
	}

	@Override
	public pizzaTypeForm getPizzaFormTypeById(int PizTypeId) {
		return PTDao.getPizzaTypeById(PizTypeId);
	}

	@Override
	public PizzaType getPizzaTypeById(int PizTypeId) {
		return PTDao.findById(PizTypeId).get();
	}

	@Override
	public PizzaType updatePizzaTypeDetails(PizzaTypeDTO npt) {
		
		Pizza p = PDao.findById(npt.getPizza().getPid()).get();
		p.setPname(npt.getPizza().getPname());
		p.setDescription(npt.getPizza().getDescription());
		p.setCategory(npt.getPizza().getCategory());
		p = PDao.save(p);
		
//		p = PDao.findById(npt.getPizza().getPid()).get();
		PizzaType pizzaTypeNew = PTDao.findById(npt.getPizTypeId()).get();
		pizzaTypeNew.setStatus(npt.getStatus());
		pizzaTypeNew.setSize(npt.getSize());
		pizzaTypeNew.setPrice(npt.getPrice());
		pizzaTypeNew.setPizza(p);
//		System.out.println(pizzaTypeNew);
		
		return PTDao.save(pizzaTypeNew);
	}
}
