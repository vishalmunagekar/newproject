package com.app.SpringBootJWT;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootJwt1Application {

	public static void main(String[] args) {
		System.out.println("Main method works...");
		SpringApplication.run(SpringBootJwt1Application.class, args);
	}

}
