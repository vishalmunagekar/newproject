package com.app.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Home {
	@RequestMapping("/hello")
	public String firstPage() {
		System.out.println("firstPage() works...");
		return "Hello World";
	}
}
