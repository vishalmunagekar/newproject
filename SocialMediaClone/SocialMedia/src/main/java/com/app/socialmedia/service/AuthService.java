package com.app.socialmedia.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.socialmedia.dto.AuthenticationResponse;
import com.app.socialmedia.dto.LoginRequest;
import com.app.socialmedia.dto.OtpDto;
import com.app.socialmedia.dto.RefreshTokenRequest;
import com.app.socialmedia.dto.RegistrationRequestDto;
import com.app.socialmedia.model.Gender;
import com.app.socialmedia.model.User;
import com.app.socialmedia.model.UserProfile;
import com.app.socialmedia.nosqlmodel.RegistrationVerificationToken;
import com.app.socialmedia.nosqlservice.RegistrationVerificationService;
import com.app.socialmedia.repository.UserProfileRepository;
import com.app.socialmedia.repository.UserRepository;
import com.app.socialmedia.secutiy.JWTProvider;

import lombok.AllArgsConstructor;

@Service
@Transactional
@AllArgsConstructor
public class AuthService {
	private final UserRepository userRepository;
	private final AuthenticationManager authenticationManager;
	private final PasswordEncoder passwordEncoder;
	private final JWTProvider jwtProvider;
	private final RegistrationVerificationService registrationVerificationService;
	private final MailSenderService mailSenderService;
	private final RefreshTokenService refreshTokenService;
	private final OtpService otpService;
	private final UserProfileRepository userProfileRepository;
	
//	@Value("${pictureurl}")
//	private String profilePicture;
	
	
	public boolean signUp(RegistrationRequestDto registrationRequestDto) {
		try {
			User user = User.builder()
				.username(registrationRequestDto.getUsername())
				.email(registrationRequestDto.getEmail())
				.gender(Gender.valueOf(registrationRequestDto.getGender()))
				.isActivated(false)
				.password(this.passwordEncoder.encode(registrationRequestDto.getPassword()))
				.build();
			this.userRepository.save(user);
			RegistrationVerificationToken verificationToken = this.registrationVerificationService.saveVarificationToken();
			this.mailSenderService.sendVerificationMail(verificationToken, user);
			return true;
		} catch (RuntimeException e) {
			System.out.println(e);
			return false;
		}
	}
	
	public void activateUserAccount(Long userId) {
		User user = this.userRepository.findByUserId(userId);
		UserProfile userProfile = UserProfile.builder()
					.profilePicture("http://localhost:3000/resources/1598812900065One_User_Orange.png")
					.user(user)
					.build();
		this.userProfileRepository.save(userProfile);
		user.setActivated(true);
		this.userRepository.save(user);
	}
	
	public AuthenticationResponse login(LoginRequest loginRequest) {
		System.out.println(loginRequest);
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
									loginRequest.getUsername(), 
									loginRequest.getPassword()));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String token = this.jwtProvider.generateToken(authentication);
		
		return AuthenticationResponse.builder()
						.authenticationToken(token)
						.refreshToken(this.refreshTokenService.generateRefreshToken().getToken())
						.expiresAt(this.jwtProvider.getExpirationDate())
						.username(loginRequest.getUsername()).build();
	}
	
	public AuthenticationResponse refreshJwtToken(RefreshTokenRequest refreshTokenRequest) {
		this.refreshTokenService.validateRefreshToken(refreshTokenRequest.getRefreshToken());
		return AuthenticationResponse.builder()
				.authenticationToken(this.jwtProvider.generateTokenWithUsername(refreshTokenRequest.getUsername()))
				.expiresAt(this.jwtProvider.getExpirationDate())
				.refreshToken(refreshTokenRequest.getRefreshToken())
				.username(refreshTokenRequest.getUsername())
				.build();
	}
	
	public OtpDto forgotPassword(String username) {
		return this.otpService.saveOtp(username);
	}

	public boolean ValidateOtp(OtpDto otpDto) {
		return this.otpService.ValidateOtp(otpDto);
	}
}
