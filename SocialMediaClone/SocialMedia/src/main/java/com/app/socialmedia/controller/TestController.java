package com.app.socialmedia.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.socialmedia.dto.CommentRequest;
import com.app.socialmedia.dto.PostRequest;
import com.app.socialmedia.model.Post;
import com.app.socialmedia.nosqlmodel.FriendRelation;
import com.app.socialmedia.nosqlmodel.PostComment;
import com.app.socialmedia.nosqlrepository.TestRepository;
import com.app.socialmedia.repository.PostRepository;
import com.app.socialmedia.service.PostService;

@RestController
@RequestMapping("/api/test")
public class TestController {
	
	public TestController() {
		System.out.println("Test Controller works...");
	}
	
	@Autowired
	private TestRepository testRepository;
	
	@GetMapping("/hello")
	public void testHello() {
	}
	
	@PostMapping("/add/{friendId}")
	public String add(@PathVariable Long friendId, @RequestBody FriendRelation friendRelation) {
		friendRelation.addFrindInList(friendId);
		testRepository.save(friendRelation);
		return "successfully Added";
	}
	
	@GetMapping("/get")
	public List<FriendRelation> getAll() {
		System.out.println(UUID.randomUUID());
		return testRepository.findAll();
	}
	 
	@PutMapping("/update")
	public FriendRelation update(@RequestBody FriendRelation friendRelation) {
		FriendRelation newObject = testRepository.findByUserId(friendRelation.getUserId());
		newObject.getFriends().add(12L);
		newObject.getFriends().add(13L);
		return testRepository.save(newObject);
	}
	
	
}
