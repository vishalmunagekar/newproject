package com.app.socialmedia.nosqlrepository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.app.socialmedia.nosqlmodel.PostComment;

public interface PostCommentRepositoy extends  MongoRepository<PostComment, String> {
	Optional<PostComment> findByPostCommentId(String postCommentId);
}
