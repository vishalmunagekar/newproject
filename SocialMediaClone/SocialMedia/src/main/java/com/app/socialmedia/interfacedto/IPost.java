package com.app.socialmedia.interfacedto;

import java.util.Date;
import java.util.List;



public interface IPost {
	Long	getPostId();
	String	getImageUrl();
	String	getCaption();
	String getUsername();
	Date	getPublishedDate();
	String	getPostCommentId();
	List<UserProfileForm> getUserProfile();
}

interface UserProfileForm{
	Integer getUserProfileId();
	//String getFirstName();
	//String getLastName();
	String getProfilePicture();
	//Address getAddress();
	//Date getBirthDate();
	//String getOccupation();
	//String getMobileNo();
}