package com.app.socialmedia.nosqlmodel;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "PostComment")
public class PostComment {
	@Id
	private String postCommentId;
	private List<Comment> commets = new ArrayList<>();
	
	public boolean addNewComment(Comment comment) {
		return this.commets.add(comment);
	}
}
