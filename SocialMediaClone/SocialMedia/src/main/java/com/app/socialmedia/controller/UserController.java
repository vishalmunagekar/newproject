package com.app.socialmedia.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.socialmedia.model.UserProfile;
import com.app.socialmedia.service.UserService;


@RestController
@RequestMapping("/api/user")
public class UserController {

	@Autowired
	private UserService userService;
	
	@GetMapping("/profile/{username}")
	public ResponseEntity<UserProfile> getUserProfileByUsername(@PathVariable String username) {
		return new ResponseEntity<UserProfile>(this.userService.getUserProfileByUsername(username), HttpStatus.OK);
	}
}
