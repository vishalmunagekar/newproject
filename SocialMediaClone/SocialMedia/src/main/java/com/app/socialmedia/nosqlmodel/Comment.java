package com.app.socialmedia.nosqlmodel;

import java.time.Instant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Comment {
	private String comment;
	private String username;
	private Instant published; //Comment Posted Time Stamp
}
