package com.app.socialmedia.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.socialmedia.interfacedto.IPost;
import com.app.socialmedia.model.Post;

public interface PostRepository extends JpaRepository<Post, Long>{
	@Query("SELECT p FROM Post p WHERE p.postId = ?1")
	IPost findByPostId(Long postId);
	
	@Query("SELECT p FROM Post p")
	List<IPost> getAllPosts();
	
	List<IPost> findByUsername(String username);
	
}
