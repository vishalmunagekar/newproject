package com.app.socialmedia.nosqlrepository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.app.socialmedia.nosqlmodel.FriendRelation;

public interface TestRepository extends MongoRepository<FriendRelation, String> {
	FriendRelation findByUserId(Long userId);
}
