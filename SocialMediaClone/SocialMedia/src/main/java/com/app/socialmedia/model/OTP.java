package com.app.socialmedia.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class OTP {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long otpId;
	private int otpNumber;
	private String otpToken;
	private String username;
}
