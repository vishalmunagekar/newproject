package com.app.socialmedia.service;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.app.socialmedia.model.User;
import com.app.socialmedia.repository.UserRepository;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {
	
	private final UserRepository userRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<User> optionalUser = this.userRepository.findByUsername(username);
		
		User user = optionalUser.orElseThrow(()-> new UsernameNotFoundException("No user " +
                "Found with username : " + username));
		
		return new org.springframework.security.core.userdetails.User(
				user.getUsername(), 
				user.getPassword(), 
				user.isActivated(), 
				true, 
				true, 
				true, 
				getGrantedAuthorities("USER"));
	}
	
	public Collection<? extends GrantedAuthority> getGrantedAuthorities(String role){
		return Collections.singletonList(new SimpleGrantedAuthority(role));
	}
}
