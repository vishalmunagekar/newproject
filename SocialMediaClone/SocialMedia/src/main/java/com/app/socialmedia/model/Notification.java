package com.app.socialmedia.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Notification {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long notificationId;
	
	private boolean notificationStatus;
	
	@Enumerated(EnumType.STRING)
	@Column(length = 15)
	private NotificationType notificationType;
	
	@ManyToOne
	@JoinColumn(name = "reciver",referencedColumnName = "userId")
	private User reciver;
	
	@OneToOne
	@JoinColumn(name = "sender",referencedColumnName = "userId")
	private User sender;
	
}
