package com.app.socialmedia.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.socialmedia.model.OTP;

public interface OtpRepository extends JpaRepository<OTP, Long> {
	Optional<OTP> findByOtpNumber(int otpNumber);
	Optional<OTP> findByOtpToken(String otpToken);
}
