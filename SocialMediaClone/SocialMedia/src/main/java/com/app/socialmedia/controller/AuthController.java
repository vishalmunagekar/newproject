package com.app.socialmedia.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.app.socialmedia.dto.AuthenticationResponse;
import com.app.socialmedia.dto.ChangePasswordRequest;
import com.app.socialmedia.dto.LoginRequest;
import com.app.socialmedia.dto.OtpDto;
import com.app.socialmedia.dto.RefreshTokenRequest;
import com.app.socialmedia.dto.RegistrationRequestDto;
import com.app.socialmedia.nosqlservice.RegistrationVerificationService;
import com.app.socialmedia.service.AuthService;
import com.app.socialmedia.service.OtpService;


@RestController
@RequestMapping("/api/auth")
public class AuthController {
	
	@Autowired
	private AuthService authService;
	@Autowired
	private OtpService otpService;
	
	
	@Autowired
	private RegistrationVerificationService registrationVerificationService;
	
	@PostMapping("/signup")
	public ResponseEntity<Boolean> singUp(@RequestBody RegistrationRequestDto registrationRequestDto){
		System.out.println(registrationRequestDto);
			if (authService.signUp(registrationRequestDto)) {
				return new ResponseEntity<Boolean>(true, HttpStatus.OK);
			}
		return new ResponseEntity<Boolean>(false, HttpStatus.BAD_REQUEST);
	}
	
	@PostMapping("/login")
	public ResponseEntity<AuthenticationResponse> login(@RequestBody LoginRequest loginRequest) {
		
		return new ResponseEntity<AuthenticationResponse>(this.authService.login(loginRequest),HttpStatus.ACCEPTED);
	}
	
	@GetMapping("/mailverification")
	public ResponseEntity<String> RegistrationVerification(@RequestParam Long userId, @RequestParam String verificationToken) {
		System.out.println(verificationToken);
		if(this.registrationVerificationService.isVerificationTokenValid(verificationToken)) {
			authService.activateUserAccount(userId);
			return new ResponseEntity<String>("<h2>Your account has been successfully activated..Thank you!</h2>",HttpStatus.OK);
		} else {
			return new ResponseEntity<String>("<h2>Something went wrong, Please try again...!!<h2>", HttpStatus.NOT_ACCEPTABLE);
		}
	}
	
	@PostMapping("/refresh/token")
	public ResponseEntity<AuthenticationResponse> refreshJwtToken(@RequestBody RefreshTokenRequest refreshTokenRequest){
		return new ResponseEntity<AuthenticationResponse>(this.authService.refreshJwtToken(refreshTokenRequest), HttpStatus.OK);
	}
	
	
	@GetMapping("/forgot/password/{username}")
	public ResponseEntity<OtpDto> forgotPassword(@PathVariable String username) {
		System.out.println(username);
		OtpDto otpDto = this.authService.forgotPassword(username);
		if(otpDto.getOtpToken() != null) {
			return new ResponseEntity<OtpDto>( otpDto, HttpStatus.OK); //"OTP has been sent on your registered mail id"
		}
		return new ResponseEntity<OtpDto>( new OtpDto(), HttpStatus.FORBIDDEN );
	}
	
	@PostMapping("/forgot/password/verification")
	public ResponseEntity<Boolean> forgotPasswordVerification(@RequestBody OtpDto otpDto) {
		if(this.authService.ValidateOtp(otpDto))
			return new ResponseEntity<Boolean>(true, HttpStatus.OK);
		return new ResponseEntity<Boolean>(false, HttpStatus.FORBIDDEN);
	}
	
	@PostMapping("/change/password")
	public ResponseEntity<Boolean> changePassword(@RequestBody ChangePasswordRequest changePasswordRequest){
		if(changePasswordRequest.getPassword().equals(changePasswordRequest.getConfirmPassword())) {
			if (this.otpService.changePassword(changePasswordRequest)) {
				return new ResponseEntity<Boolean>(true, HttpStatus.OK);
			}
			return new ResponseEntity<Boolean>(false, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Boolean>(false , HttpStatus.NOT_FOUND);
	}
	
}
