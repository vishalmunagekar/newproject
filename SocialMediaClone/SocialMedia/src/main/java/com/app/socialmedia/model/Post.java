package com.app.socialmedia.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Post {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long postId;
	
	private String imageUrl;
	
	@Column(length = 500)
	private String caption;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date publishedDate;
	
	private String postCommentId;
	
	private String username;
	
	@ManyToOne
	@JoinColumn(name = "userProfileId",referencedColumnName = "userProfileId")
	private UserProfile userProfile;
	
}