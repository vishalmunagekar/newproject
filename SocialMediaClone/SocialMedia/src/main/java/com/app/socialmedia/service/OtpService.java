package com.app.socialmedia.service;

import java.lang.Math;
import java.util.Optional;
import java.util.UUID;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.app.socialmedia.dto.ChangePasswordRequest;
import com.app.socialmedia.dto.OtpDto;
import com.app.socialmedia.model.OTP;
import com.app.socialmedia.model.User;
import com.app.socialmedia.repository.OtpRepository;
import com.app.socialmedia.repository.UserRepository;

import lombok.AllArgsConstructor;

@Service
@Transactional
@AllArgsConstructor
public class OtpService {
	
	private final OtpRepository otpRepository;
	private final UserRepository userRepository;
	private final MailSenderService mailSenderService;
	private final PasswordEncoder passwordEncoder;
	
	public OtpDto saveOtp(String username) {
		try {
			User user = userRepository.findByUsername(username)
					.orElseThrow(()-> new RuntimeException("User not found...!!"));
			
			OTP otp = OTP.builder()
							.otpNumber(generateOtp())
							.otpToken(UUID.randomUUID().toString())
							.username(user.getUsername()).build();
			otp = otpRepository.save(otp);
			mailSenderService.sendOtpNumber(otp, user);
			return OtpDto.builder()
						.otpToken(otp.getOtpToken())
						.username(otp.getUsername()).build();
			
		} catch (RuntimeException e) {
			System.out.println(e);
			return new OtpDto();
		}
	}
	
	public boolean ValidateOtp(OtpDto otpDto){
		try {
			OTP otp = this.otpRepository.findByOtpNumber(otpDto.getOtpNumber())
							.orElseThrow(()-> new RuntimeException("OTP not found..!!"));
			
			
			return otp.getOtpToken().equals(otpDto.getOtpToken());
		} catch (RuntimeException e) {
			System.out.println(e);
			return false;
		}
	}
	
	public int generateOtp() {
		Double random = Math.random() * 1000000;
		return (int) Math.round(random);
	}
	
	public boolean changePassword(ChangePasswordRequest changePasswordRequest) {
		try {
			User user = this.userRepository.findByUsername(changePasswordRequest.getUsername())
								.orElseThrow(()-> new RuntimeException("User not found !!"));
			
			OTP otp = this.otpRepository.findByOtpToken(changePasswordRequest.getOtpToken())
								.orElseThrow(()-> new RuntimeException("OTP Token not valid !!"));
			
			user.setPassword(this.passwordEncoder.encode(changePasswordRequest.getPassword()));
			this.userRepository.save(user);
			return true;
		} catch (RuntimeException e) {
			System.out.println(e);
			return false;
		}
	}
		
}
