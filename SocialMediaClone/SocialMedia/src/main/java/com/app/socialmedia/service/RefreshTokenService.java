package com.app.socialmedia.service;

import java.time.Instant;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.socialmedia.model.RefreshToken;
import com.app.socialmedia.repository.RefreshTokenRepository;

@Service
public class RefreshTokenService {
	
	@Autowired
	private RefreshTokenRepository refreshTokenRepository;
	
	public RefreshToken generateRefreshToken() {
		RefreshToken refreshToken = RefreshToken.builder()
									.token(UUID.randomUUID().toString())
									.createdDate(Instant.now())
									.build();
		
		return this.refreshTokenRepository.save(refreshToken);
	}
	
	public void validateRefreshToken(String token) {
        refreshTokenRepository.findByToken(token)
                .orElseThrow(() -> new RuntimeException("Invalid refresh Token"));
    }
	
	public void deleteRefreshToken(String token) {
		this.refreshTokenRepository.deleteByToken(token);
	}
}
