package com.app.socialmedia.service;

import org.springframework.stereotype.Service;

import com.app.socialmedia.model.User;
import com.app.socialmedia.model.UserProfile;
import com.app.socialmedia.repository.UserProfileRepository;
import com.app.socialmedia.repository.UserRepository;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class UserService {

	private final UserRepository userRepository ;
	private final UserProfileRepository userProfileRepository;
	
	
	public UserProfile getUserProfileByUsername(String username) {
		User user = this.userRepository.findByUsername(username)
							.orElseThrow(()-> new RuntimeException("User not found!"));
		return this.userProfileRepository.findByUser(user).orElseThrow(()-> new RuntimeException("UserProfile not found!"));
	}
	
}
