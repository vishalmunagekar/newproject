package com.app.socialmedia.nosqlmodel;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "FriendRelation")
public class FriendRelation {
	@Id
	private String friendRelationId;
	private Long userId;
	private List<Long> friends = new ArrayList<>();
	
	public boolean addFrindInList(Long friendId) {
		return friends.add(friendId);
	} 
}
