package com.app.socialmedia.nosqlrepository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.app.socialmedia.nosqlmodel.RegistrationVerificationToken;

public interface RegistrationVerificationRepository extends MongoRepository<RegistrationVerificationToken, String>{
	RegistrationVerificationToken findByVerificationToken(String verificationToken);
}
