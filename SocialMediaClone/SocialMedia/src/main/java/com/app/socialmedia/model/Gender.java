package com.app.socialmedia.model;

public enum Gender {
	MALE, FEMALE, OTHER;
}
