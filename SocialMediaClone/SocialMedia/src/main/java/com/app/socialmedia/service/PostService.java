package com.app.socialmedia.service;


import java.time.Instant;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.app.socialmedia.dto.CommentRequest;
import com.app.socialmedia.dto.PostRequest;
import com.app.socialmedia.interfacedto.IPost;
import com.app.socialmedia.model.Post;
import com.app.socialmedia.model.User;
import com.app.socialmedia.model.UserProfile;
import com.app.socialmedia.nosqlmodel.Comment;
import com.app.socialmedia.nosqlmodel.PostComment;
import com.app.socialmedia.nosqlrepository.PostCommentRepositoy;
import com.app.socialmedia.repository.PostRepository;
import com.app.socialmedia.repository.UserProfileRepository;
import com.app.socialmedia.repository.UserRepository;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class PostService {

	private final PostRepository postRepository;
	private final PostCommentRepositoy postCommentRepositoy;
	private final UserRepository userRepository;
	private final UserProfileRepository  userProfileRepository;
	
	public boolean addNewPost(PostRequest postRequest){
		try {
			PostComment newPostComment = postCommentRepositoy.save(new PostComment());
			System.out.println(newPostComment);
			User user = this.userRepository.findByUsername(postRequest.getUsername())
											.orElseThrow(()-> new RuntimeException("User not Found!!"));
			UserProfile userProfile = this.userProfileRepository.findByUser(user)
											.orElseThrow(()-> new RuntimeException("UserProfile not found!!"));
			
			Post post = Post.builder()
						.imageUrl(postRequest.getImageUrl())
						.caption(postRequest.getCaption())
						.publishedDate(new Date())
						.userProfile(userProfile)
						.username(user.getUsername())
						.postCommentId(newPostComment.getPostCommentId())
						.build();
			postRepository.save(post);
			return true;
		} catch (RuntimeException e) {
			System.out.println(e);
			return false;
		}
	}
	
	public boolean addNewComment(CommentRequest commentRequest) {
		try {
			PostComment postComment = postCommentRepositoy.findByPostCommentId(commentRequest.getPostCommentId())
										.orElseThrow(()-> new RuntimeException("PostComment not found!!"));
			Comment comment = Comment.builder()
										.comment(commentRequest.getComment())
										.username(commentRequest.getUsername())
										.published(Instant.now())
										.build();
			postComment.addNewComment(comment);
			postCommentRepositoy.save(postComment);
			return true;
		} catch (RuntimeException e) {
			System.out.println(e);
			return false;
		}
	}
	
	public IPost getPostByPostId(Long postId) {
		return this.postRepository.findByPostId(postId);
								//.orElseThrow(()-> new RuntimeException("Post not found!!"));
	}
	
	public PostComment getPostCommentsById(String postCommentId) {
		return postCommentRepositoy.findByPostCommentId(postCommentId)
						.orElseThrow(()-> new RuntimeException("PostComment not found!!"));
	}
	
	public List<IPost> getAllPosts() {
		return this.postRepository.getAllPosts();
								//.orElseThrow(()-> new RuntimeException("Posts not found!!"));
	}
	
	public List<IPost> getPostsByUsername(String username){
		return this.postRepository.findByUsername(username);
	}
}
