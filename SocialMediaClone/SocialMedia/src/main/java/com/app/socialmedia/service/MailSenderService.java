package com.app.socialmedia.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.app.socialmedia.model.OTP;
import com.app.socialmedia.model.User;
import com.app.socialmedia.nosqlmodel.RegistrationVerificationToken;

@Service
public class MailSenderService {
	@Autowired
	private JavaMailSender mailSender;
	@Value("${host.ip}")
	private String hostIp;
	
	@Async
	public void sendVerificationMail(RegistrationVerificationToken verificationToken, User user) {
		System.out.println(this.hostIp);
		
		String msg = "Hello "	+ user.getUsername() 
								+ " Click on this URL to Activate your Account : " 
								+ "http://"+ this.hostIp + ":9090/api/auth/mailverification?"
								+ "userId=" + user.getUserId()
								+ "&verificationToken=" + verificationToken.getVerificationToken();
		
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setTo(user.getEmail());
		mailMessage.setSubject("Registration to BookFace");
		mailMessage.setText(msg);
		try
		{
			mailSender.send(mailMessage);
		}
		catch (MailException e) 
		{
			System.out.println("inside mail exception");
			e.printStackTrace();
		}
	}
	
	@Async
	public void sendOtpNumber(OTP otp, User user) {
		String msg = "Hello "	+ user.getUsername() 
								+ ", Your OTP for reset password is " 
								+ otp.getOtpNumber()
								+ " use this OTP for change your password";
		
		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setTo(user.getEmail());
		mailMessage.setSubject("OTP for reset password");
		mailMessage.setText(msg);
		try
		{
			mailSender.send(mailMessage);
		}
		catch (MailException e) 
		{
			System.out.println("inside mail exception");
			e.printStackTrace();
		}
	}
}
