package com.app.socialmedia.nosqlservice;

import java.time.Instant;
import java.util.UUID;

import org.springframework.stereotype.Service;

import com.app.socialmedia.nosqlmodel.RegistrationVerificationToken;
import com.app.socialmedia.nosqlrepository.RegistrationVerificationRepository;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class RegistrationVerificationService {
	private final RegistrationVerificationRepository registrationVerificationRepository;
	
	public RegistrationVerificationToken saveVarificationToken() {
		RegistrationVerificationToken verificationToken = RegistrationVerificationToken.builder()
																	.verificationToken(UUID.randomUUID().toString())
																	.registrationDate(Instant.now())
																	.build();
		return this.registrationVerificationRepository.save(verificationToken);
	}
	
	public boolean isVerificationTokenValid(String VerificationToken) {
		try {
			boolean isValid = false;
			RegistrationVerificationToken StoredToken = this.registrationVerificationRepository.findByVerificationToken(VerificationToken);
			isValid = StoredToken.getVerificationToken()
												.equals(VerificationToken);
			if(isValid) {
				this.registrationVerificationRepository.delete(StoredToken);
			}
			return isValid;
		} catch (RuntimeException e) {
			System.out.println("Verification token is invalid");
			return false;
		}
	}
}
