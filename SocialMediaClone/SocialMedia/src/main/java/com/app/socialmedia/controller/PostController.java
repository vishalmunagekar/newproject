package com.app.socialmedia.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.app.socialmedia.dto.CommentRequest;
import com.app.socialmedia.dto.PostRequest;
import com.app.socialmedia.interfacedto.IPost;
import com.app.socialmedia.model.Post;
import com.app.socialmedia.nosqlmodel.PostComment;
import com.app.socialmedia.service.PostService;

@Controller
@RequestMapping("/api/post")
public class PostController {
	
	@Autowired
	private PostService postService;
	
	@PostMapping("/new/post")
	public ResponseEntity<Boolean> addNewPost(@RequestBody PostRequest postRequest){
		if (this.postService.addNewPost(postRequest)) {
			return new ResponseEntity<Boolean>(true ,HttpStatus.OK);
		}
		return new ResponseEntity<Boolean>(false ,HttpStatus.NOT_FOUND);
	}
	
	@PostMapping("/new/comment")
	public ResponseEntity<Boolean> addNewComment(@RequestBody CommentRequest commentRequest){
		if (this.postService.addNewComment(commentRequest)) {
			return new ResponseEntity<Boolean>(true ,HttpStatus.OK);
		}
		return new ResponseEntity<Boolean>(false ,HttpStatus.NOT_FOUND);
	}
	
	@GetMapping("/{postId}")
	public ResponseEntity<IPost> getPostByPostId(@PathVariable Long postId) {
		return new ResponseEntity<IPost>(this.postService.getPostByPostId(postId), HttpStatus.OK);
	}
	
	@GetMapping("/all/posts")
	public ResponseEntity<List<IPost>> getAllFriendsPosts() {
		return new ResponseEntity<List<IPost>>(this.postService.getAllPosts(), HttpStatus.OK);
	}
	
	@GetMapping("/comment/{postCommentId}")
	public ResponseEntity<PostComment> getPostCommentsByPostId(@PathVariable String postCommentId) {
		return new ResponseEntity<PostComment>(this.postService.getPostCommentsById(postCommentId), HttpStatus.OK);
	}
	
	@GetMapping("/all/{username}")
	public ResponseEntity<List<IPost>> getPostsByUsername(@PathVariable String username){
		return new ResponseEntity<List<IPost>>(this.postService.getPostsByUsername(username), HttpStatus.OK);
	}
}
