-- MySQL dump 10.13  Distrib 8.0.17, for Linux (x86_64)
--
-- Host: localhost    Database: springboot
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `address` (
  `addrid` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(30) NOT NULL,
  `flatno` int(11) NOT NULL,
  `pincode` int(11) NOT NULL,
  `street` varchar(30) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`addrid`),
  KEY `FKbfn1l96xkepprn0hffrwr99dl` (`uid`),
  CONSTRAINT `FKbfn1l96xkepprn0hffrwr99dl` FOREIGN KEY (`uid`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (1,'ICHAL',420,416115,'SHASTRI',1),(2,'KOP',786,411556,'GANDHI',2),(3,'JAMKHED',416,415520,'NEHARU',3),(4,'KOP',113,416115,'TARARANI',1);
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_details`
--

DROP TABLE IF EXISTS `order_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `quantity` int(11) NOT NULL,
  `oid` int(11) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKrocfvt7nbxmbuvomvlac7dxcq` (`oid`),
  KEY `FKdpqmk0n1oovgpag11pjhds70y` (`pid`),
  CONSTRAINT `FKdpqmk0n1oovgpag11pjhds70y` FOREIGN KEY (`pid`) REFERENCES `pizzas` (`pid`),
  CONSTRAINT `FKrocfvt7nbxmbuvomvlac7dxcq` FOREIGN KEY (`oid`) REFERENCES `orders` (`oid`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_details`
--

LOCK TABLES `order_details` WRITE;
/*!40000 ALTER TABLE `order_details` DISABLE KEYS */;
INSERT INTO `order_details` VALUES (1,2,501,101),(2,3,501,102),(3,1,502,105),(4,1,502,106),(5,2,503,102),(6,2,503,107),(7,1,504,109),(8,3,504,101),(9,2,505,102),(10,1,505,105);
/*!40000 ALTER TABLE `order_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `orders` (
  `oid` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  `total_amt` float NOT NULL,
  `total_qty` int(11) NOT NULL,
  `addrid` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`oid`),
  KEY `FKr7p86qilt82ceg20vc8jeywmf` (`addrid`),
  KEY `FK58x4l9shxmkb7pismj4ilt7pj` (`uid`),
  CONSTRAINT `FK58x4l9shxmkb7pismj4ilt7pj` FOREIGN KEY (`uid`) REFERENCES `user` (`id`),
  CONSTRAINT `FKr7p86qilt82ceg20vc8jeywmf` FOREIGN KEY (`addrid`) REFERENCES `address` (`addrid`)
) ENGINE=InnoDB AUTO_INCREMENT=506 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (501,'2019-01-01','Delivered',1500,5,1,1),(502,'2019-01-02','Delivered',800,2,2,2),(503,'2019-01-03','Delivered',600,4,3,3),(504,'2019-01-05','NotDelivered',600,4,4,4),(505,'2019-01-17','NotDelivered',680,3,1,5);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pizzas`
--

DROP TABLE IF EXISTS `pizzas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pizzas` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(15) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image` longblob,
  `pname` varchar(60) DEFAULT NULL,
  `price` float NOT NULL,
  `size` varchar(15) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pizzas`
--

LOCK TABLES `pizzas` WRITE;
/*!40000 ALTER TABLE `pizzas` DISABLE KEYS */;
INSERT INTO `pizzas` VALUES (101,'VEG','Baby Corn, Black Olives, Green Capsicum, Jalapeno, Red Capsicum',_binary '/assets/veg_pizzas/Corn_&_Cheese.jpg','Veg Exotica',100,'PERSONAL','AVAILABLE'),(102,'VEG','Green Capsicum, Masala Paneer, Masala Soya Chunk, Onion, Red Paprika',_binary '/assets/veg_pizzas/Deluxe_Veggie.jpg','Paneer Soya Supreme',200,'FAMILY','AVAILABLE'),(103,'VEG','Black Olives, Green And Yellow Zucchini, Jalapeno, Mushroom, Red Capsicum',_binary '/assets/veg_pizzas/Digital_Veggie_Paradise_olo_266x265.jpg','Veggie Italiano',300,'MEDIUM','AVAILABLE'),(104,'VEG','Black Olives, Green Capsicum, Mushroom, Onion, Red Paprika, Sweet Corn',_binary '/assets/veg_pizzas/Double_Cheese_Margherita.jpg','Veggie Supreme',400,'PERSONAL','AVAILABLE'),(105,'NONVEG','Chicken Pepperoni, Green And Yellow Zucchini, Jalapeno, Smoked Chicken',_binary '/assets/non_veg_pizzas/IndianChickenTikka.jpg','Chicken Exotica',500,'FAMILY','NOTAVAILABLE'),(106,'NONVEG','Black Olives, Chicken Pepperoni, Chicken Sausage, Green Capsicum, Mushroom',_binary '/assets/non_veg_pizzas/Non-Veg_Supreme.jpg','Chicken Italiano',250,'MEDIUM','NOTAVAILABLE'),(107,'NONVEG','Chicken Tikka, Lebanese Chicken, Schezwan Chicken Meat Ball',_binary '/assets/non_veg_pizzas/Pepper_Barbeque.jpg','Chicken Supreme',350,'PERSONAL','AVAILABLE'),(108,'NONVEG','Chicken Sausage, Green Capsicum, Lebanese Chicken, Onion, Red Paprika, Schezwan Chicken Meat Ball',_binary '/assets/non_veg_pizzas/Pepper_Barbeque_&_Onion.jpg','Triple Chicken Feast',450,'FAMILY','NOTAVAILABLE'),(109,'VEG','Cheese',_binary '/assets/veg_pizzas/Corn_&_Cheese.jpg','Margherita',550,'MEDIUM','NOTAVAILABLE'),(110,'NONVEG','Chicken Sausage, Onion',_binary '/assets/veg_pizzas/Corn_&_Cheese.jpg','Chicken Sausage',150,'PERSONAL','NOTAVAILABLE'),(111,'VEG','Green Capsicum, Masala Paneer, Masala Soya Chunk, Onion, Red Paprika',_binary '/assets/veg_pizzas/Deluxe_Veggie.jpg','Paneer PizzaHot Special',600,'FAMILY','AVAILABLE'),(113,'NONVEG','Green Capsicum, Masala Paneer, Masala Soya Chunk, Onion, Red Paprika',_binary '/assets/veg_pizzas/Deluxe_Veggie.jpg','vishal Special',100,'FAMILY','AVAILABLE');
/*!40000 ALTER TABLE `pizzas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dob` date DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `mob_no` varchar(10) DEFAULT NULL,
  `pass` varchar(60) DEFAULT NULL,
  `role` varchar(15) DEFAULT NULL,
  `uname` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UKgq8i8bjk7osiatf3ha4c1789u` (`email`,`mob_no`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'1995-01-17','vishal@gmail.com','7843052772','zxcvbnm','VENDOR','vishalm'),(2,'1995-01-17','akash@gmail.com','7843052771','zxcvbnm','USER','Akashw'),(3,'1997-01-07','Ashish@gmail.com','7843052773','zxcvbnm','USER','AshishR'),(4,'1995-01-17','Ajinkya@gmail.com','9960735906','zxcvbnm','USER','Ajinkya'),(5,'1995-01-17','Aniket@gmail.com','9960735906','zxcvbnm','USER','Aniket'),(14,'1993-10-26','vishu@gmail.com','7843052772','zxcvbnm','USER','vishum');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-27 13:00:29
