package com.app.redisdb.repository;

import java.util.Map;

import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import com.app.redisdb.model.User;


@Service
public class UserRepositoryImpl implements UserRepository {
	
	private final RedisTemplate<String, Object> redisTemplate;
	private final HashOperations<String, Long, User> hashOperations;
	
	
	public UserRepositoryImpl(RedisTemplate<String, Object> redisTemplate ) {
		this.redisTemplate = redisTemplate;
		this.hashOperations = this.redisTemplate.opsForHash();
	}

	@Override
	public void save(User user) {
		this.hashOperations.put("USER", user.getId(), user);
	}

	@Override
	public Map<Long, User> findAll() {
		return this.hashOperations.entries("USER");
	}

	@Override
	public User findById(Long id) {
		return this.hashOperations.get("USER", id);
	}

	@Override
	public void update(User user) {
		this.hashOperations.put("USER", user.getId(), user);
	}

	@Override
	public void delete(Long id) {
		this.hashOperations.delete("USER", id);
	}

}
