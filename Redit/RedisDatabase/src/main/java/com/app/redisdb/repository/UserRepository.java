package com.app.redisdb.repository;

import java.util.Map;

import com.app.redisdb.model.User;

public interface UserRepository {
	
	void save(User user);
    Map<Long, User> findAll();
    User findById(Long id);
    void update(User user);
    void delete(Long id);
}
