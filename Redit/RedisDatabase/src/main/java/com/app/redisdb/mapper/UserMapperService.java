package com.app.redisdb.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.app.redisdb.model.User;


@Component
public class UserMapperService {
	
	@Autowired(required = true)
	private UserMapper userMapper;
	
	public User getUser(UserDto userDto) {
		return this.userMapper.getUserFromDto(userDto);
	}

	public UserDto getUserDtoFromUser(User user) {
		return this.userMapper.getUserDtoFromUser(user);
	} 
}
