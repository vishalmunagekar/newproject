package com.app.redisdb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.redisdb.mapper.UserDto;
import com.app.redisdb.mapper.UserMapperService;
import com.app.redisdb.model.User;

@RestController
@RequestMapping("/api/mapper")
public class MapperCotroller {
	
	@Autowired
	private UserMapperService mapperService;


	@PostMapping("/user")
	public User getUserFromDto(@RequestBody UserDto userDto) {
		return this.mapperService.getUser(userDto);
	}

	@PostMapping("/userdto")
	public UserDto getUserDtoFromUser(@RequestBody User user) {
		return this.mapperService.getUserDtoFromUser(user);
	}
}
