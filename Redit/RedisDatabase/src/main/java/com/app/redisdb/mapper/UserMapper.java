package com.app.redisdb.mapper;

import org.mapstruct.Mapper;

import com.app.redisdb.model.User;

@Mapper(componentModel = "spring")
public interface UserMapper {
	
	User getUserFromDto(UserDto userDto);
	
	
	UserDto getUserDtoFromUser(User user);
}
