package com.app.redisdb.controller;

import java.util.Map;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.redisdb.model.User;
import com.app.redisdb.repository.UserRepository;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class TestController {
	
	private final UserRepository userRepository;
	
	@GetMapping("/get/all")
	public Map<Long, User> getAllUsers() {
		return this.userRepository.findAll();
	}
	
	@GetMapping("/get/{id}")
	public User getUserById(@PathVariable Long id) {
		return this.userRepository.findById(id);
	}
	
	@PostMapping("/save")
	public String addNewUser(@RequestBody User user) {
		this.userRepository.save(user);
		return "User added successfully..!!";
	}
	
	@DeleteMapping("/delete/{id}")
	public String deleteUserByID(@PathVariable Long id) {
		this.userRepository.delete(id);
		return "Deleted..!!";
	}
	
	@PutMapping("/update")
	public String updateUser(@RequestBody User user) {
		this.userRepository.update(user);
		return "User Updated successfully..!!";
	}
	
}
