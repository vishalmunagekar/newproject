package com.app.redisdb.model;

import java.io.Serializable;

import com.app.redisdb.mapper.Address;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {//implements Serializable {
	
	//private static final long serialVersionUID = 1L;
	
	private Long id;
	private String name;
	private Long salary;
	private Address address;
}
