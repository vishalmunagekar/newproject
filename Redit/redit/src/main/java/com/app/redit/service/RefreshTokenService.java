package com.app.redit.service;

import java.time.Instant;
import java.util.UUID;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.redit.model.RefreshToken;
import com.app.redit.repository.RefreshTokenRepository;

import lombok.AllArgsConstructor;

@Service
@Transactional
@AllArgsConstructor
public class RefreshTokenService {
	private final RefreshTokenRepository refreshTokenRepository;
	
	public RefreshToken generateRefreshToken() {
		RefreshToken refreshToken = new RefreshToken();
		refreshToken.setToken(UUID.randomUUID().toString());
		
		System.out.println(refreshToken.getToken());
		
		refreshToken.setCreatedDate(Instant.now());
		return this.refreshTokenRepository.save(refreshToken);
	}
	
	public void validateRefreshToken(String token) {
		this.refreshTokenRepository.findByToken(token).orElseThrow(
				()-> new RuntimeException("Token not found!"));
	}
	
}
