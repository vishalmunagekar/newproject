package com.app.redit.service;

import java.time.Instant;

import javax.validation.Valid;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.redit.dto.AuthenticationResponse;
import com.app.redit.dto.LoginRequest;
import com.app.redit.dto.RefreshTokenRequest;
import com.app.redit.dto.RegisterRequest;
import com.app.redit.model.User;
import com.app.redit.repository.UserRepository;
import com.app.redit.security.JWTProvider;

import lombok.AllArgsConstructor;

@Service
@Transactional
@AllArgsConstructor
public class AuthService {
	
	private final UserRepository userRepository;
	private final AuthenticationManager authenticationManager;
	private final PasswordEncoder passwordEncoder;
	private final JWTProvider jwtProvider;
	private final RefreshTokenService refreshTokenService;
	
	
	public void signUp(RegisterRequest registerRequest) {
		User user = new User();
		user.setUsername(registerRequest.getUsername());
        user.setEmail(registerRequest.getEmail());
        user.setPassword(passwordEncoder.encode(registerRequest.getPassword()));
        user.setCreated(Instant.now());
        user.setEnabled(true);
        
        this.userRepository.save(user);
	}
	
	
	public AuthenticationResponse logIn(LoginRequest loginRequest) {
		System.out.println("Service login method works...");
		Authentication authentication = authenticationManager
					.authenticate(new UsernamePasswordAuthenticationToken(
							loginRequest.getUsername(),
							loginRequest.getPassword()));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String token = this.jwtProvider.generateToken(authentication);
		
		return AuthenticationResponse.builder()
				.authenticationToken(token)
				.refreshToken(this.refreshTokenService.generateRefreshToken().getToken())
				.username(loginRequest.getUsername())
				.build();
	}


	public AuthenticationResponse refreshToken(RefreshTokenRequest refreshTokenRequest) {
		this.refreshTokenService.validateRefreshToken(refreshTokenRequest.getRefreshToken());
		String JwtToken = this.jwtProvider.generateTokenWithUsername(refreshTokenRequest.getUsername());
		return AuthenticationResponse.builder()
				.authenticationToken(JwtToken)
				.refreshToken(refreshTokenRequest.getRefreshToken())
				.expiresAt(Instant.now().plusSeconds(60))
				.username(refreshTokenRequest.getUsername())
				.build();
	}	
}
