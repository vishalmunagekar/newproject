package com.app.redit.service;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.redit.model.User;
import com.app.redit.repository.UserRepository;

import lombok.AllArgsConstructor;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	
	@Autowired
	private UserRepository userRepository;

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) {
		Optional<User> optionalUser = this.userRepository.findByUsername(username);
	
		System.out.println("loadUserByUsername method works...");
		User user = optionalUser.orElseThrow(()-> new UsernameNotFoundException("No user " +
                "Found with username : " + username));
		
		return new org.springframework.security.core.userdetails.User(
				user.getUsername(), 
				user.getPassword(), 
				user.isEnabled(), 
				true, 
				true, 
				true, 
				getGrantedAuthorities("USER"));
	}

	public Collection<? extends GrantedAuthority> getGrantedAuthorities(String role){
		return Collections.singletonList(new SimpleGrantedAuthority(role));
	}

}
