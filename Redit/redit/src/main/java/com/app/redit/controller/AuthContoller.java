package com.app.redit.controller;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.redit.dto.AuthenticationResponse;
import com.app.redit.dto.LoginRequest;
import com.app.redit.dto.RefreshTokenRequest;
import com.app.redit.dto.RegisterRequest;
import com.app.redit.service.AuthService;

import lombok.AllArgsConstructor;

@RestController
@AllArgsConstructor
@RequestMapping("/api/auth")
public class AuthContoller {

	private final AuthService authService;
	
	@PostMapping("/signup")
	public ResponseEntity<String> signUp(@RequestBody RegisterRequest registerRequest){
		System.out.println(registerRequest.toString());
		this.authService.signUp(registerRequest);
		return new ResponseEntity<String>("User registration successful", HttpStatus.OK);
	}
	
	@PostMapping("/login")
	public ResponseEntity<AuthenticationResponse> logIn(@RequestBody LoginRequest loginRequest){
		System.out.println("Controller login method works...");
		return new ResponseEntity<AuthenticationResponse>(authService.logIn(loginRequest),HttpStatus.OK);
	}
	
	@GetMapping("/test")
	public String test(){
		return "OK";
	}
	
	@PostMapping("/refresh/token")
	public ResponseEntity<AuthenticationResponse> refreshToken(@Valid @RequestBody RefreshTokenRequest refreshTokenRequest){
		System.out.println(refreshTokenRequest);
		return new ResponseEntity<AuthenticationResponse>(
				this.authService.refreshToken(refreshTokenRequest),
				HttpStatus.OK);
	}
	
}