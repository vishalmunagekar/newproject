package com.app.redit.model;

import java.util.Arrays;

public enum VoteType {
	UPVOTE(1), DOWNVOTE(-1);
	
	private int direction;
	
	private VoteType(int direction) {
		
	}
	
	public Integer getDirection() {
		return direction;
	}
	
	public static VoteType lookUp(Integer direction) {
		Arrays.stream(VoteType.values())
			.filter(values -> values.getDirection().equals(direction))
			.findAny()
			.orElseThrow(()-> new RuntimeException("Vote not Found...!!!"));
		return null;
	}
}
