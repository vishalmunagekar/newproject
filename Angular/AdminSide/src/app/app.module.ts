import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule, routingComponent } from './app-routing.module';
import { AppComponent } from './app.component';
import { PizzaService } from './services/pizza.service';
import { OrderService } from './services/order.service';
import { UserService } from './services/user.service';
import { FormsModule } from '@angular/forms';
import { DataCommunicationService } from './services/data-communication.service';
import { AddressService } from './services/address.service';

@NgModule({
  declarations: [
    AppComponent,
    routingComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, 
    HttpClientModule,
    FormsModule
  ],
  providers: [HttpClientModule, PizzaService, OrderService, UserService, DataCommunicationService, AddressService],
  bootstrap: [AppComponent]
})
export class AppModule { }
