export interface Pizza {
  pname: string;
  category: string;
  image: string;
  pizzaTypes: PizzaType[];
  description: string;
  pid: number;
}

export interface PizzaType {
  pizTypeId: number;
  status: string;
  size: string;
  price: number;
}
