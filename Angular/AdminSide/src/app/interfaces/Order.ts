export interface Order {
  oid: number;
  date: Date;
  totalAmt: number;
  status: string;
  totalQty: number;
}
