export interface user {
    id: number;
    role: string;
    email: string;
    pass: string;
    uname: string;
    mobNo: string;
    dob: string;
}