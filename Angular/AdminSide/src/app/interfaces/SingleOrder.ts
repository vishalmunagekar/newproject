export interface SingleOrder {
  date: Date;
  orderDetails: OrderDetail[];
  oid: number;
  totalAmt: number;
  totalQty: number;
  oaddr: Oaddr;
  status: string;
}

export interface Pizza {
  pid: number;
  pname: string;
  category: string;
  image: string;
  description: string;
}

export interface PizzaType {
  size: string;
  pizTypeId: number;
  pizza: Pizza;
  price: number;
  status: string;
}

export interface OrderDetail {
  id: number;
  quantity: number;
  totalPrice: number;
  pizzaType: PizzaType;
}

export interface Oaddr {
  addrid: number;
  street: string;
  flatno: number;
  city: string;
  pincode: number;
}



