export interface PizzaType {
  size: string;
  pizTypeId: number;
  pizza: Pizza;
  price: number;
  status: string;
}

export interface Pizza {
  pid: number;
  pname: string;
  category: string;
  image: string;
  description: string;
}
