import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AddPizzaComponent } from './components/add-pizza/add-pizza.component';
import { LoginComponent } from './components/Auth/login/login.component';
import { ProfileComponent } from './components/Auth/profile/profile.component';
import { UpdatePizzaComponent } from './components/update-pizza/update-pizza.component';
import { AuthGuard } from './auth.guard';
import { NotFoundComponent } from './components/not-found/not-found.component';


const routes: Routes = [
  { path : "", component : HomeComponent, canActivate: [AuthGuard]},
  { path : "Login", component : LoginComponent },
  { path : "Profile", component : ProfileComponent, canActivate: [AuthGuard]},
  { path : "Home", component : HomeComponent, canActivate: [AuthGuard] },
  { path : "UpdatePizza", component : UpdatePizzaComponent, canActivate: [AuthGuard]},
  { path : "AddPizza", component : AddPizzaComponent, canActivate: [AuthGuard]},
  { path : "**", component : NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingComponent = [
  HomeComponent,
  AddPizzaComponent,
  LoginComponent,
  ProfileComponent,
  UpdatePizzaComponent,
  AddPizzaComponent,
  NotFoundComponent
];