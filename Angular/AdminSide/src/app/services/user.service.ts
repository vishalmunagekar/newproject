import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor( private http: HttpClient ) { }

  userLogin(credentials: { email: string, pass:string })
  {
      return this.http.post("http://localhost:9090/pizzahot/user/login", credentials )
  }

  getUserProfile(uid:any)
  {
      return this.http.get("http://localhost:9090/pizzahot/user/" + uid);
  }

  addNewUser(user:any)
  {
    return this.http.post("http://localhost:9090/pizzahot/user/add", user);
  }
}
