import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Pizza } from '../interfaces/Pizza';
import { PizzaType } from '../interfaces/PizzaType';

@Injectable({
  providedIn: 'root'
})
export class PizzaService {

  constructor( public http : HttpClient ) { }

  fetchAllPizzas()
  {
    return this.http.get("http://localhost:9090/pizzahot/pizza");
  }

  getPizzaById(pid:number){
    return this.http.get("http://localhost:9090/pizzahot/pizza/" + pid);
  }

  updatePizzaTypeDetails(newPizzaType:PizzaType)
  {
    return this.http.put("http://localhost:9090/pizzahot/pizzaType/update",newPizzaType);
  }
  addPizzaDetails(pizza:Pizza){
    return this.http.post("http://localhost:9090/pizzahot/pizza/add",pizza);
  }
  getPizzaTypeById(pizzaTypeId:number){
    return this.http.get("http://localhost:9090/pizzahot/pizzaType/" + pizzaTypeId);
  }
}
