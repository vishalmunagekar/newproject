import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataCommunicationService {
  public subscriptionForLogin = new BehaviorSubject(false);
 // public cartSubject = new BehaviorSubject(new cart());
  public RefreshSubject = new BehaviorSubject(undefined);
  constructor() {
  }

  LoginData(id: any) {
    localStorage.setItem("USERID", id);
    this.subscriptionForLogin.next(true);
  }

  ngOnInit() {

  }
}
