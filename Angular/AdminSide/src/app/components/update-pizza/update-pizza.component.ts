import { Component, OnInit, Pipe } from '@angular/core';
import { Pizza } from 'src/app/interfaces/Pizza';
import { PizzaService } from 'src/app/services/pizza.service';
import { Router } from '@angular/router';
import { PizzaType } from 'src/app/interfaces/PizzaType';

@Component({
  selector: 'app-update-pizza',
  templateUrl: './update-pizza.component.html',
  styleUrls: ['./update-pizza.component.css']
})
export class UpdatePizzaComponent implements OnInit {
  pizzas: Pizza[];
  singlePizza: PizzaType;
  AllPizzas: Pizza[];
  selectedRow: number;
  isLoaded: boolean = true;
  selectedNavBtn: number;
  tempPizzas: Pizza[];
  selectedPage: number = 0;
  fadeIn: boolean;

  constructor(private pizzaService: PizzaService,
    private router: Router) { }


  getPizzaDetail(pizTypeId:number) {
    //console.log(pizTypeId);
    this.fadeIn = true;
    this.pizzaService.getPizzaTypeById(pizTypeId).subscribe((result: PizzaType) => {
      this.singlePizza = result;
      this.selectedRow = this.singlePizza.pizza.pid;
      setTimeout(() => {
        this.fadeIn = false;
      }, 1000);
    });
  }

  updatePizzaDetails() {
   console.log(this.singlePizza);
    this.pizzaService.updatePizzaTypeDetails(this.singlePizza).subscribe((result: PizzaType) => {
      console.log(result);
      this.pizzas = this.pizzas.map((piz: Pizza) => {
        if (piz.pid == this.singlePizza.pizza.pid){
          piz.image = this.singlePizza.pizza.image; piz.pname = this.singlePizza.pizza.pname;
          piz.description = this.singlePizza.pizza.description; piz.category = this.singlePizza.pizza.category;
          piz.pizzaTypes = piz.pizzaTypes.map((pizType:PizzaType)=>{
            if(pizType.pizTypeId == this.singlePizza.pizTypeId)
            {
              pizType.price = this.singlePizza.price;
              pizType.status = this.singlePizza.status;
            }
            return pizType;
          })
        }
        return piz;
      });
      // this.AllPizzas = this.AllPizzas.map((piz: Pizza) => {
      //   if (piz.pid == pizza.pid)
      //     piz = pizza;
      //   return piz;
      // });
    });
  }

  loadAllPizzas() {
    this.tempPizzas = this.AllPizzas;
    this.pizzas = this.tempPizzas.slice(0, 5);
    this.selectedNavBtn = 0;
    this.selectedPage = 0;
  }

  loadAvailablePizzas() {
    this.tempPizzas = this.AllPizzas.filter((pizz: Pizza) => {
      return pizz.pizzaTypes[0].status == 'AVAILABLE'
    })
    this.pizzas = this.tempPizzas.slice(0, 5);
    this.selectedNavBtn = 1;
    this.selectedPage = 0;
  }

  loadNotAvailablePizzas() {
    this.tempPizzas = this.AllPizzas.filter((pizz: Pizza) => {
      return pizz.pizzaTypes[0].status == 'NOTAVAILABLE'
    })
    this.pizzas = this.tempPizzas.slice(0, 5);
    this.selectedNavBtn = 2;
    this.selectedPage = 0;

  }

  loadVegPizzas() {
    this.tempPizzas = this.AllPizzas.filter((pizz: Pizza) => {
      return pizz.category == 'VEG'
    })
    this.pizzas = this.tempPizzas.slice(0, 5);
    this.selectedNavBtn = 3;
    this.selectedPage = 0;
  }

  loadNonVegPizzas() {
    this.tempPizzas = this.AllPizzas.filter((pizz: Pizza) => {
      return pizz.category == 'NONVEG'
    })
    this.pizzas = this.tempPizzas.slice(0, 5);
    this.selectedNavBtn = 4;
    this.selectedPage = 0;
  }

  pagination(i: number) {
    this.selectedPage = i;
    let x = i * 5;
    this.pizzas = this.tempPizzas.slice(x, x + 5);
    console.log(x, x + 5);

  }

  ngOnInit(): void {
    this.pizzaService.fetchAllPizzas().subscribe((result: Pizza[]) => {
      this.AllPizzas = result;
      this.tempPizzas = result;
      this.pizzas = this.tempPizzas.slice(0, 5);

      setTimeout(() => {
        this.isLoaded = false;
      }, 100);

      this.pizzaService.getPizzaTypeById(this.AllPizzas[0].pizzaTypes[0].pizTypeId).subscribe((result: PizzaType) => {
        this.singlePizza = result;
        this.selectedRow = this.singlePizza.pizza.pid;
      });
    });
    this.selectedNavBtn = 0;
  }
}
