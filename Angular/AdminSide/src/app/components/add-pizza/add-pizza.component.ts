import { Component, OnInit } from '@angular/core';
import { Pizza } from 'src/app/interfaces/Pizza';
import { PizzaService } from 'src/app/services/pizza.service';

@Component({
  selector: 'app-add-pizza',
  templateUrl: './add-pizza.component.html',
  styleUrls: ['./add-pizza.component.css']
})
export class AddPizzaComponent implements OnInit {
  singlePizza:Pizza
  constructor(private pizzaService:PizzaService) { }

  AddNewPizza(newPizza:Pizza){
    this.pizzaService.addPizzaDetails(newPizza).subscribe((result:Pizza)=>{
      console.log(result);
    });
  }
  ngOnInit(): void {
  }

}
