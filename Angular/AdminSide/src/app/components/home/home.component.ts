import { Component, OnInit } from '@angular/core';
import { Order } from 'src/app/interfaces/Order'
import { SingleOrder } from 'src/app/interfaces/SingleOrder'
import { OrderService } from 'src/app/services/order.service';
import { AddressService } from 'src/app/services/address.service';
import { DataCommunicationService } from 'src/app/services/data-communication.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  orders: Order[] = [];
  allOrders: Order[] = [];
  singleOrder: SingleOrder;
  selectedRow: number;
  selectedNavBtn: number = 0;
  isLoaded: boolean = false;
  tempOrder:any[] = [];
  selectedPage:number = 0;
  isRefresh:boolean;
  fadeIn:boolean;

  constructor(public orderService: OrderService,
    public addressService: AddressService,
    private dataService: DataCommunicationService) { }

  getOrderDetail(oid: number) {
   // this.fadeIn = false;
    this.fadeIn = true;
    this.selectedRow = oid;

    this.orderService.getOrderById(oid)
      .subscribe((result: any) => {
        this.singleOrder = result;
        setTimeout(() => {
          this.fadeIn = false;
        }, 1000);
      });

  }

  loadAllOrders() {
    this.selectedNavBtn = 0;
    this.selectedPage = 0;
    this.tempOrder = this.allOrders;
    this.orders = this.tempOrder.slice(0,5);
  }

  loadDeliveredOrders() {
    this.selectedNavBtn = 1;
    this.selectedPage = 0;
    this.tempOrder = this.allOrders.filter((odr: any) => (odr.status == "Delivered"));
    this.orders = this.tempOrder.slice(0,5);
  }

  loadNotDeliveredOrders() {
    this.selectedNavBtn = 2;
    this.selectedPage = 0;
    this.tempOrder = this.allOrders.filter((odr: any) => (odr.status == "NotDelivered"));
    this.orders = this.tempOrder.slice(0,5);
  }

  loadCancelledOrders() {
    this.selectedPage = 0;
    this.selectedNavBtn = 3;
    this.tempOrder = this.allOrders.filter((odr: any) => (odr.status == "Cancelled"));
    this.orders = this.tempOrder.slice(0,5);
  }

  pagination(i:number)
  {
    this.selectedPage = i;
    let x = i*5;
    this.orders = this.tempOrder.slice(x,x+5);
    console.log(x , x+5);
  }

  ngOnInit() {
    this.orderService.getAllOrders()
      .subscribe((result: any) => {
        this.allOrders = result;
        this.tempOrder = result;
        this.orders = this.tempOrder.slice(0,5);

        this.selectedRow = this.allOrders[0].oid;

        // setTimeout(() => {
        //   this.isLoaded = false;
        // }, 1000);

        this.orderService.getOrderById(this.allOrders[0].oid)
          .subscribe((result: any) => {
            this.singleOrder = result;
          });
      });

      this.dataService.RefreshSubject.subscribe((result:boolean)=>{
        this.isRefresh = result;
        console.log(this.isRefresh);
      })
  }

}
