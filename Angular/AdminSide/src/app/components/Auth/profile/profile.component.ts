import { Component, OnInit } from '@angular/core';
import { user } from 'src/app/interfaces/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  user: user;

  constructor(public userService: UserService) {

  }

  ngOnInit() {
    this.userService.getUserProfile(localStorage.USERID)
      .subscribe((result: any) => {
        this.user = result;
      });
  }

}
