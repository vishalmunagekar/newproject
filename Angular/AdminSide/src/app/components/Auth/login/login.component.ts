import { Component, OnInit } from '@angular/core';
import { user } from 'src/app/interfaces/user';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { DataCommunicationService } from 'src/app/services/data-communication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: user;
  constructor(private router: Router,
    private userService: UserService, private dataService : DataCommunicationService) { }

  loginFormValidation(credentials: any) {
    this.userService.userLogin(credentials)
      .subscribe((result: any) => {
        if (result != null) {
          alert("Login successfully...");
          this.user = result;
          this.dataService.LoginData(this.user.id);
          this.router.navigate(['Home']);
            this.dataService.RefreshSubject.next(true);
        } else {
          alert("Somthing went Wrong...!!! pls try again..");
          this.router.navigate(['Login']);
        }
      });
  }

  ngOnInit(): void {
  }

}
