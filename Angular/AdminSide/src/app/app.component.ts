import { Component } from '@angular/core';
import { DataCommunicationService } from './services/data-communication.service';
import { AuthGuard } from './auth.guard';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'AdminSide';
  isLogged: boolean = false;

  constructor(public authGuard: AuthGuard,
    public dataService: DataCommunicationService) {
    if (localStorage.USERID === undefined)
      localStorage.setItem("USERID", "0");

    this.dataService.subscriptionForLogin.subscribe((data: boolean) => {
      this.isLogged = data;
    });
  }

  logOut() {
    localStorage.USERID = "0";
    this.dataService.subscriptionForLogin.next(false);
  }

}
