import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(public userService : UserService) { }

  signUpForm(userData:any)
  {
    this.userService.addNewUser(userData)
    .subscribe((result:any)=>{
      if(result !== null)
        alert("Successfully Sing Up..!!!")
    },(error:any)=>{
      if(error !== null)
        alert("Somthing went Wrong...!! Pls try again..")
    });
  }

  ngOnInit(): void {
  }

}
