import { Component, OnInit, NgModule } from '@angular/core';
import { AuthGuard } from './auth.guard';
import { DataCommunicationService } from './data-communication.service';
import { cart } from './interfaces/cart';
import { PizzaService } from './pizza.service';
import { pizza } from './interfaces/pizza';
import { CartPizza } from './interfaces/CartPizza';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  isLogged: boolean = false;
  newCart: cart;

  constructor(public authGuard: AuthGuard,
    public dataService: DataCommunicationService,
    public pizzaService:PizzaService) {
    if (localStorage.USERID === undefined)
      localStorage.setItem("USERID", "0");

    this.dataService.subscriptionForLogin.subscribe((data: boolean) => {
      this.isLogged = data;
    });
    let cart = JSON.parse(localStorage.getItem("CART"));
    //console.log(cart);
    this.dataService.cartSubject.next(cart);
  }

  logOut() {
    localStorage.USERID = "0";
    this.dataService.subscriptionForLogin.next(false);
    this.dataService.cartSubject.next(new cart());
    localStorage.setItem("CART", `{"totalAmt":0,"totalQty":0,"pizzas":[]}`)
  }

  ngOnInit() {
    this.dataService.cartSubject.subscribe((result: cart) => {
      this.newCart = result;
    });

    this.pizzaService.fetchAllPizzas()
      .subscribe((result: pizza[]) => {
        result.map((piz:pizza)=>{
          for (let i = 0; i < 3; i++) {
            piz.pizzaTypes[i].isTypeAdded = false;
          }
          result = result.map((piz: pizza) => {
            this.newCart.pizzas.map((cartP: CartPizza) => {
              if (piz.pname == cartP.pname) {
                for (let i = 0; i < 3; i++) {
                  if (piz.pizzaTypes[i].size == cartP.pizzaTypes.size) {
                    piz.pizzaTypes[i].isTypeAdded = true;
                  }
                }
              }
            });
            return piz;
          });
        });
       // console.log(result);
        this.dataService.allPizzaSubject.next(result);
      });
  }
}
