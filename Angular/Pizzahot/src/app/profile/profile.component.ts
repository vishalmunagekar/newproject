import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { user } from '../interfaces/user';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user: user;

  constructor(public userService: UserService) {

  }

  ngOnInit() {
    this.userService.getUserProfile(localStorage.USERID)
      .subscribe((result: any) => {
        this.user = result;
      });
  }
}
