import { Component, OnInit, AfterViewInit, AfterViewChecked } from '@angular/core';
import { PizzaService } from '../pizza.service';
import { DataCommunicationService } from '../data-communication.service';
import { cart } from '../interfaces/cart';
import { pizza } from '../interfaces/pizza';
import { Router } from '@angular/router';
import { CartPizza } from '../interfaces/CartPizza';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  pizzas: pizza[];
  pizzaView: pizza[];
  selectedRow: number = 1;
  cart: cart = new cart();
  isLoaded: boolean = true;
  searchPizza: string;
  isLogged: boolean = false;
  cntr: number = 3;
  loadingSybl: boolean = false;
  selectedItem: number;


  constructor(public pizzaService: PizzaService,
    public dataService: DataCommunicationService,
    private router: Router) {
  }

  allPizzasLoad() {
    this.pizzaView = this.pizzas;
    this.selectedRow = 1;
  }

  nonVegPizzasLoad() {
    this.pizzaView = this.pizzas.filter((piz: any) => (piz.category == "NONVEG"));
    this.selectedRow = 2;

  }

  vegPizzasLoad() {
    this.pizzaView = this.pizzas.filter((piz: any) => (piz.category == "VEG"));
    this.selectedRow = 3;
  }

  topPizzasLoad() {
    this.pizzaView = this.pizzas.filter((piz: any) => (piz.category == "TOP"));
    this.selectedRow = 4;
  }

  addToCart(pizza: pizza, ptid: number) {
    this.dataService.subscriptionForLogin.subscribe((data: boolean) => {
      this.isLogged = data;
    });

    if (!this.isLogged) {
      this.router.navigate(['Login']);
    }
    else {
      let i = this.cart.pizzas.findIndex((piz: CartPizza) => {
        return piz.pname === pizza.pname && piz.pizzaTypes.size === pizza.pizzaTypes[ptid].size;
      });

      //console.log(i)
      if (i == -1) {
        pizza.quantity = 1;
        pizza.pizzaTypes[ptid].isTypeAdded = true;
        pizza.totalPrice = pizza.pizzaTypes[ptid].price;
        let newPizza: CartPizza = this.ConvertPizzaToCartPizza(pizza, ptid);
        this.cart.pizzas.push(newPizza);
      }
      else {
        this.cart.pizzas[i].quantity += 1;
        this.cart.pizzas[i].totalPrice = this.cart.pizzas[i].quantity * this.cart.pizzas[i].pizzaTypes.price;
      }
      this.cart.totalAmt = 0;
      this.cart.totalQty = 0;
      this.cart.pizzas.map((p: CartPizza) => {
        this.cart.totalAmt += p.totalPrice;
        this.cart.totalQty += p.quantity;
      });
      this.dataService.cartSubject.next(this.cart);
      localStorage.setItem("CART", JSON.stringify(this.cart));
    }
  }

  ConvertPizzaToCartPizza(pizza: pizza, index: number): CartPizza {
    let newCartPizza: CartPizza = new CartPizza();
    newCartPizza.pid = pizza.pid; newCartPizza.image = pizza.image; newCartPizza.description = pizza.description;
    newCartPizza.pname = pizza.pname; newCartPizza.quantity = pizza.quantity; newCartPizza.totalPrice = pizza.totalPrice;
    newCartPizza.pizzaTypes = pizza.pizzaTypes[index];
    return newCartPizza;
  }

  ngOnInit() {
    this.dataService.cartSubject.subscribe((result: cart) => {
      this.cart = result;
    });

    this.dataService.PizzasObservabale
      .subscribe((result: pizza[]) => {
        this.pizzas = result
        setTimeout(() => {
          this.isLoaded = false;
        }, 1200);
       this.pizzaView = this.pizzas;
      });
    // let options = {
    //   root: null,
    //   rootMargin: "0px",
    //   threshold: 0.75
    // };
    // let observer = new IntersectionObserver((entries) => {
    //   if (entries[0].isIntersecting && this.pizzas !== undefined) {
    //     this.loadingSybl = true;
    //     setTimeout(() => {
    //       this.loadingSybl = false;
    //       this.pizzaView = this.pizzaView.concat(this.pizzas.slice(this.cntr, this.cntr += 3));
    //     }, 2000);
    //   }
    // }, options)
    // observer.observe(document.querySelector("footer"));
  }
}
