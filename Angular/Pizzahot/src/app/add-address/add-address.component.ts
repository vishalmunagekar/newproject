import { Component, OnInit, Inject } from '@angular/core';
import { AddressService } from '../address.service';
import { DOCUMENT } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: '.app-add-address',
  templateUrl: './add-address.component.html',
  styleUrls: ['./add-address.component.css']
})
export class AddAddressComponent implements OnInit {
  constructor(public addressService: AddressService,
    @Inject(DOCUMENT) private document: Document) { }

  addNewAddress(newAddress:any, closeModal:HTMLElement )
  {
    this.addressService.addNewAddress(localStorage.USERID, newAddress)
    .subscribe((result:any)=>{
      console.log(result);
      closeModal.click()
      this.document.location.reload();
    });
  }


  ngOnInit(): void {
  }

}
