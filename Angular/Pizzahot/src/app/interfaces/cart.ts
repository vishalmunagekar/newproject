import { CartPizza } from './CartPizza';

export class cart {
    totalAmt: number;
    totalQty:number;
    pizzas:CartPizza[];
    addrid:number;
    uid:number;

    constructor() {
        this.totalAmt = 0;
        this.totalQty = 0;
        this.pizzas = [];
    }
}
