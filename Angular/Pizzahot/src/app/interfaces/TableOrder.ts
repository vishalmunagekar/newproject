export interface Oaddr {
  addrid: number;
  street: string;
  flatno: number;
  city: string;
  pincode: number;
}

export interface TableOrder {
  oid: number;
  date: Date;
  totalAmt: number;
  status: string;
  totalQty: number;
  oaddr: Oaddr;
}
