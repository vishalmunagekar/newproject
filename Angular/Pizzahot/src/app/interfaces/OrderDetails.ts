import { Oaddr } from './TableOrder';

  export interface Pizza {
      pid: number;
      pname: string;
      category: string;
      image: string;
      description: string;
  }

  export interface PizzaType {
      size: string;
      status: string;
      pizTypeId: number;
      pizza: Pizza;
      price: number;
  }

  export interface OrderDetail {
      id: number;
      quantity: number;
      totalPrice: number;
      pizzaType: PizzaType;
  }

  export interface SingleOrder {
      date: Date;
      status: string;
      orderDetails: OrderDetail[];
      oid: number;
      totalAmt: number;
      totalQty: number;
      oaddr: Oaddr;
  }
