export interface oaddr{
    addrid:number;
    flatno: number;
    street: string;
    city: string;
    pincode: number;
};