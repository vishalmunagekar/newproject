import { PizzaType } from './PizzaType';

export class pizza{
    pid:number;
    pname: string;
    image:string;
    description: string;
    pizzaTypes:PizzaType[];
    quantity:number;
    totalPrice:number;

    constructor(){
      this.pizzaTypes = [];
    }
}
