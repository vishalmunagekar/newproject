import { PizzaType } from './PizzaType';

export class CartPizza {
    pid:number;
    pname: string;
    image:string;
    description: string;
    pizzaTypes:PizzaType
    quantity:number;
    totalPrice:number;
    constructor(){    }
}
