import { oaddr } from "./oaddr";

export interface singleOrder{
    oid: number;
    totalAmt: number;
    date:string;
    orderDetails: any[];
    totalQty:number;
    status:string;
    oaddr:oaddr;
};