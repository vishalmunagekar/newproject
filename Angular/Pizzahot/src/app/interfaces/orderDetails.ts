export class orderDetails {
    quantity: number;
    pizza: newPizza;
    totalPrice: number;

    constructor(qty:number, pizza:newPizza, totalPrice:number) {
        this.quantity = qty;
        this.pizza = pizza;
        this.totalPrice = totalPrice;
    }
}

class newPizza {
    pid: number;

    constructor(pid:number) {
        this.pid = pid;
    }
}
