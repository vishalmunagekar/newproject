export class PostNewOrder {
  totalAmt: number;
  totalQty: number;
  pizzas: PizzaTypes[];
  addrid: number;
  uid: string;
  constructor() {
    this.pizzas = [];
   }
}

export class PizzaTypes {
  pizTypeId: number; //Yes
  quantity: number; //Yes
  totalPrice: number; //yes
  constructor() { }
}
