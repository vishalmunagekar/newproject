import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ProfileComponent } from './profile/profile.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { CheckOutComponent } from './check-out/check-out.component';
import { AddAddressComponent } from './add-address/add-address.component';
import { VendorHomeComponent } from './vendor-home/vendor-home.component';
import { HomeComponent } from './home/home.component';
import { CartComponent } from './cart/cart.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { MyOrderComponent } from './my-order/my-order.component';
import { AuthGuard } from './auth.guard';


const routes: Routes = [
  { path : "", component : HomeComponent},
  { path : "Login", component : LoginComponent },
  { path : "Signup", component : SignupComponent },
  { path : "Profile", component : ProfileComponent, canActivate: [AuthGuard] },
  { path : "EditProfile", component : EditProfileComponent, canActivate: [AuthGuard] },
  { path : "CheckOut", component : CheckOutComponent, canActivate: [AuthGuard] },
  { path : "AddAddress", component: AddAddressComponent, canActivate: [AuthGuard]},
  { path : "VendorHome", component: VendorHomeComponent, canActivate: [AuthGuard]},
  { path : "Home", component : HomeComponent },
  { path : "MyOrder", component : MyOrderComponent, canActivate: [AuthGuard] },
  { path : "**", component : NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [
  LoginComponent,
  SignupComponent,
  ProfileComponent,
  EditProfileComponent,
  CheckOutComponent,
  AddAddressComponent,
  VendorHomeComponent,
  HomeComponent,
  CartComponent,
  NotFoundComponent,
  MyOrderComponent
]