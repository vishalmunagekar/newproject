import { Component, OnInit } from '@angular/core';
import { OrderService } from '../order.service';
import { SingleOrder } from 'src/app/interfaces/OrderDetails'
import { TableOrder } from 'src/app/interfaces/TableOrder'

@Component({
  selector: 'app-my-order',
  templateUrl: './my-order.component.html',
  styleUrls: ['./my-order.component.css']
})
export class MyOrderComponent implements OnInit {
  myOrders: TableOrder[] = [];
  singleOrder: SingleOrder;
  selectedRow: number;
  isLoaded: boolean = true;
  Currdate = new Date();
  constructor(public orderService: OrderService) { }

  getOrderDetail(oid: number) {
    this.selectedRow = oid;
    this.orderService.getOrderById(oid)
      .subscribe((result: SingleOrder) => {
        this.singleOrder = result;
      });
  }

  getDate(date: string): string {
    let newDate = new Date(date);
    if (newDate.getDate() === this.Currdate.getDate() && newDate.getMonth() === this.Currdate.getMonth()) {
      return "Today";
    }
    else if (newDate.getDate() === (this.Currdate.getDate() - 1) && newDate.getMonth() === this.Currdate.getMonth()) {
      return "Yesterday";
    }
    return newDate.toLocaleDateString();
  }

  ngOnInit(): void {
    this.orderService.getAllOrdersByUserId(localStorage.USERID)
      .subscribe((result: TableOrder[]) => {
        console.log(result);
        if (result.length !== 0) {
          this.myOrders = result;

          setTimeout(() => {
            this.isLoaded = false;
          }, 100);

          this.orderService.getOrderById(this.myOrders[0].oid)
            .subscribe((result: SingleOrder) => {
              this.singleOrder = result;
              this.selectedRow = this.myOrders[0].oid;
            });

        }
        else {
          setTimeout(() => {
            this.isLoaded = false;
          }, 2000);

        }
      });
  }
}
