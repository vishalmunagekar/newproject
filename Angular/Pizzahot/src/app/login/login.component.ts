import { Component, OnInit, NgModule } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { user } from '../interfaces/user';
import { DataCommunicationService } from '../data-communication.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: user;
  msg:string = "";
  constructor(private router: Router,
    private userService: UserService, private dataService : DataCommunicationService) { }

  loginFormValidation(credentials: any) {
    this.userService.userLogin(credentials)
      .subscribe((success: any) => {
          console.log(success);
          this.user = success;
          this.dataService.LoginData(this.user.id);
          this.router.navigate(['Home']);
      },(response:HttpErrorResponse)=>{
        this.msg = response.error;
      });
  }

  ngOnInit(): void {
  }

}
