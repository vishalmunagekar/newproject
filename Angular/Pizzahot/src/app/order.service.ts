import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PostNewOrder } from './interfaces/PostNewOrder';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(public http: HttpClient) { }

  getAllOrders()
  {
    return this.http.get("http://localhost:9090/pizzahot/order");
  }

  getOrderById(oid:any)
  {
    return this.http.get("http://localhost:9090/pizzahot/order/"+oid);
  }

  getAllOrdersByUserId(uid:any)
  {
    return this.http.get("http://localhost:9090/pizzahot/order/userorders/"+ uid);
  }

  bookOrder(order:PostNewOrder)
  {
    return this.http.post("http://localhost:9090/pizzahot/order/add", order);
  }
}
