import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { DataCommunicationService } from './data-communication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(public router: Router, public dataService: DataCommunicationService) {
    if (this.isLogin())
      this.dataService.subscriptionForLogin.next(true);
      if(localStorage.CART == undefined)
        localStorage.setItem("CART", `{"totalAmt":0,"totalQty":0,"pizzas":[]}`)
  }

  isLogin(): boolean {
    return (localStorage.USERID !== "0")
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.isLogin())
      return true;
    else
      return false;
  }

}
