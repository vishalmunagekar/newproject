import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { user } from '../interfaces/user';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {
  user: user;
  constructor(public userService: UserService) {
  }

  updateProfile(userData: any) {
    console.log(userData);
  }

  ngOnInit(): void {
    this.userService.getUserProfile(localStorage.USERID)
      .subscribe((result:any) => {
        this.user = result;
        console.log(this.user);
      });
  }

}
