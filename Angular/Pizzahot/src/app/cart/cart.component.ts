import { Component, OnInit } from '@angular/core';
import { DataCommunicationService } from '../data-communication.service';
import { cart } from '../interfaces/cart';
import { CartPizza } from '../interfaces/CartPizza';
import { pizza } from '../interfaces/pizza';

@Component({
  selector: '.app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  cart = new cart();
  pizzasView:pizza[];

  constructor(public dataService: DataCommunicationService) {
  }

  increaseQty(pizza: CartPizza) {
    let i = this.cart.pizzas.indexOf(pizza);
    this.cart.pizzas[i].quantity += 1;
    this.cart.pizzas[i].totalPrice = this.cart.pizzas[i].quantity * this.cart.pizzas[i].pizzaTypes.price;
    this.cart.totalAmt = 0;
    this.cart.totalQty = 0;
    this.cart.pizzas.map((p: CartPizza) => {
      this.cart.totalAmt += p.totalPrice;
      this.cart.totalQty += p.quantity;
    });

    this.dataService.cartSubject.next(this.cart);
    localStorage.setItem("CART", JSON.stringify(this.cart));
  }

  decreaseQty(pizza: CartPizza) {
    let i = this.cart.pizzas.indexOf(pizza);
    console.log(i);
    if (pizza.quantity == 1) {
      this.pizzasView.map((piz:pizza)=>{
        if(piz.pid == pizza.pid){
          for(let i=0; i < 3; i++)
          {
            if(pizza.pizzaTypes.pizTypeId == piz.pizzaTypes[i].pizTypeId){
                piz.pizzaTypes[i].isTypeAdded = false;
            }
          }
        }
      });
      this.dataService.allPizzaSubject.next(this.pizzasView); //Changed isTypeAdded Property of Pizzas here
      this.cart.pizzas = this.cart.pizzas.filter((piz: CartPizza) => {  //here we remove pericular pizza from cart using Array filter
        return (piz.pizzaTypes.pizTypeId != pizza.pizzaTypes.pizTypeId);
      })
    }
    else {
      this.cart.pizzas[i].quantity -= 1;
      this.cart.pizzas[i].totalPrice = this.cart.pizzas[i].quantity * this.cart.pizzas[i].pizzaTypes.price;
    }
    this.cart.totalAmt = 0;
    this.cart.totalQty = 0;
    this.cart.pizzas.map((p: CartPizza) => {
      this.cart.totalAmt += p.totalPrice;
      this.cart.totalQty += p.quantity;
    });
    this.dataService.cartSubject.next(this.cart);
    localStorage.setItem("CART", JSON.stringify(this.cart));
  }

  ngOnInit(): void {
    this.dataService.cartSubject.subscribe((result: cart) => {
      this.cart = result;
    });
    this.dataService.allPizzaSubject.subscribe((result:pizza[])=>{
        this.pizzasView = result;
    })
  }

}
