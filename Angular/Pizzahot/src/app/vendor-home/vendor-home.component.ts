import { Component, OnInit, AfterViewInit, AfterViewChecked } from '@angular/core';
import { OrderService } from '../order.service';
import { AddressService } from '../address.service';
import { singleOrder } from 'src/app/interfaces/singleOrder'


@Component({
  selector: 'app-vendor-home',
  templateUrl: './vendor-home.component.html',
  styleUrls: ['./vendor-home.component.css']
})




export class VendorHomeComponent implements OnInit {
  orders: any[] = [];
  allOrders: any[] = [];
  singleOrder: singleOrder;
  selectedRow: number;
  selectedNavBtn: number = 0;
  isLoaded:boolean = true;

  constructor(public orderService: OrderService,
    public addressService: AddressService) { }

  getOrderDetail(oid: number) {

    this.selectedRow = oid;

    this.orderService.getOrderById(oid)
      .subscribe((result: any) => {
        this.singleOrder = result;
      });
  }

  loadAllOrders() {
    this.selectedNavBtn = 0;
    this.orders = this.allOrders;
  }

  loadDeliveredOrders() {
    this.selectedNavBtn = 1;
    this.orders = this.allOrders.filter((odr: any) => (odr.status == "Delivered"));
  }

  loadNotDeliveredOrders() {
    this.selectedNavBtn = 2;
    this.orders = this.allOrders.filter((odr: any) => (odr.status == "NotDelivered"));
  }

  loadCancelledOrders() {
    this.selectedNavBtn = 3;
    this.orders = this.allOrders.filter((odr: any) => (odr.status == "Cancelled"));
  }

  ngOnInit() {
    this.orderService.getAllOrders()
      .subscribe((result: any) => {
        this.allOrders = result;
        this.orders = this.allOrders
        this.selectedRow = this.allOrders[0].oid;

        setTimeout(() => {
          this.isLoaded = false;
        }, 2000);

        this.orderService.getOrderById(this.allOrders[0].oid)
          .subscribe((result: any) => {
            this.singleOrder = result;
          });
      });
  }
}
