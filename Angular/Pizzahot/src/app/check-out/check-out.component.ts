import { Component, OnInit } from '@angular/core';
import { cart } from '../interfaces/cart';
import { DataCommunicationService } from '../data-communication.service';
import { OrderService } from '../order.service';
import { AddressService } from '../address.service';
import { oaddr } from '../interfaces/oaddr';
import { PostNewOrder, PizzaTypes } from '../interfaces/PostNewOrder';
import { CartPizza } from '../interfaces/CartPizza';
import { Router } from '@angular/router';

@Component({
  selector: 'app-check-out',
  templateUrl: './check-out.component.html',
  styleUrls: ['./check-out.component.css']
})
export class CheckOutComponent implements OnInit {
  cart: cart;
  allAddress: oaddr[] = [];
  addrid: any;
  constructor(private dataService: DataCommunicationService,
    private orderService: OrderService,
    private addressService: AddressService,
    private router: Router) { }

  placeNewOrder(addrid: number) {
  localStorage.USERID;
//    console.log(this.cart);
    let newOrder:PostNewOrder = new PostNewOrder();
    newOrder.uid = localStorage.USERID;
    newOrder.addrid = addrid;
    newOrder.totalQty = this.cart.totalQty;
    newOrder.totalAmt = this.cart.totalAmt;
    this.cart.pizzas.forEach((pizzaType:CartPizza) => {
      let tempPizType:PizzaTypes = new PizzaTypes();
      tempPizType.pizTypeId = pizzaType.pizzaTypes.pizTypeId;
      tempPizType.quantity = pizzaType.quantity;
      tempPizType.totalPrice = pizzaType.totalPrice;
      newOrder.pizzas.push(tempPizType);
    });
    //console.log(newOrder);
    this.orderService.bookOrder(newOrder).subscribe((result) => {
      console.log(result);
      localStorage.setItem("CART", `{"totalAmt":0,"totalQty":0,"pizzas":[]}`)
      //window.location.href="http://localhost/payment.html/"+result;
      this.router.navigate(['/MyOrder']);
    });
  }

  ngOnInit(): void {
    this.dataService.cartSubject.subscribe((result: cart) => {
      this.cart = result;
    });
    this.addressService.getUsersAllAddressById(localStorage.USERID).subscribe((result: any) => {
      this.allAddress = result;
    });
  }
}
