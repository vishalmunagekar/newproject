import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  constructor(public http: HttpClient) { }

  addNewAddress(uid:any, address:any)
  {
    return this.http.post("http://localhost:9090/pizzahot/address/add/" + uid, address);
  }

  getAddressById(aid:any)
  {
    return this.http.get("http://localhost:9090/pizzahot/address/" + aid);
  }

  getUsersAllAddressById(uid:number)
  {
    return this.http.get("http://localhost:9090/pizzahot/address/alladdress/"+uid);
  }
}