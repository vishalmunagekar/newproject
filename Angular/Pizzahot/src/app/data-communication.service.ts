import { Injectable, OnInit } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { cart } from './interfaces/cart';
import { pizza } from './interfaces/pizza';
import { PizzaService } from './pizza.service';

@Injectable({
  providedIn: 'root'
})
export class DataCommunicationService implements OnInit {
  public subscriptionForLogin = new BehaviorSubject(false);
  public cartSubject = new BehaviorSubject(new cart());
  public allPizzaSubject = new BehaviorSubject<Array<pizza>>([]);
  public PizzasObservabale = this.allPizzaSubject.asObservable();
  constructor(public pizzaService: PizzaService) {
  }

  LoginData(id: any) {
    localStorage.setItem("USERID", id);
    this.subscriptionForLogin.next(true);
  }

  ngOnInit() {

  }
}
