import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PizzaService {

  constructor( public http : HttpClient ) { }

  fetchAllPizzas()
  {
    return this.http.get("http://localhost:9090/pizzahot/pizza");
  }

}
