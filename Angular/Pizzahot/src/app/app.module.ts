import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'
import { UserService } from './user.service';
import { PizzaService } from './pizza.service';
import { OrderService } from './order.service';
import { DataCommunicationService } from 'src/app/data-communication.service';

@NgModule({
  declarations: [
    AppComponent,
    routingComponents
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    Ng2SearchPipeModule
  ],
  providers: [HttpClientModule, UserService, PizzaService, OrderService, DataCommunicationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
